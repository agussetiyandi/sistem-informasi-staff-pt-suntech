<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_DataMaster extends CI_Model {

	/* =================    FUNCTION GET DATA
	=================================================================
	*/
	
	// Admin
	public function admin_list_all() {
		$q=$this->db->select('*')->get('tb_admin');
		return $q->result();
	}

	// Departement
	public function dept_list_all() {
		$q=$this->db->select('*')->get('stc_departement');
		return $q->result();
	}

	// Position
	public function position_list_all() {
		$q=$this->db->select('*')->get('stc_position');
		return $q->result();
	}
	
	// HOD
	public function hod_list_all() {
		$q=$this->db->select('*')->get('stc_hod');
		return $q->result();
	}

	public function jabatan_list_all() {
		$q=$this->db->select('*')->get('tb_jabatan');
		return $q->result();
	}

	public function bidang_list_all() {
		$q=$this->db->select('*')->get('tb_bidang');
		return $q->result();
	}

	public function pegawai_list_all() {
		$q=$this->db->select('*')
				->from('tb_pegawai as p')
				->join('tb_bidang as b', 'b.id_bidang = p.id_bidang', 'LEFT')
				->join('tb_jabatan as t', 't.id_jabatan = t.id_jabatan', 'LEFT')
				->get();
		return $q->result();
	}

	// Employee
	public function employee_list_all() {
		$q=$this->db->query("SELECT * FROM stc_employee ORDER BY emp_id DESC");
		return $q->result();
	}

	// Employee Renewal
	public function renewal_list_all() {
		// $q=$this->db->query("SELECT * FROM stc_employee WHERE emp_finish_contract >= (NOW() - INTERVAL 30 DAY)");
		$q=$this->db->query("SELECT * FROM stc_employee WHERE emp_finish_contract BETWEEN CURRENT_DATE and DATE_ADD(CURRENT_DATE, INTERVAL 30 DAY)");
		return $q->result();
	}


		// Employee (resign)
		public function empresign_list_all() {
			$q=$this->db->select('*')->get('view_employee_resign');
			return $q->result();
		}

		// Employee (finish contract)
		public function empfinish_list_all() {
			$q=$this->db->select('*')->get('view_employee_finish_contract');
			return $q->result();
		}
		
		// Employee (run away)
		public function emprunaway_list_all() {
			$q=$this->db->select('*')->get('view_employee_runaway');
			return $q->result();
		}

		// Employee (running)
		public function emprunning_list_all() {
			$q=$this->db->select('*')->get('view_employee_running');
			return $q->result();
		}		
		

	//
	public function namaizin_list_all() {
		$q=$this->db->select('*')->get('tb_namaizin');
		return $q->result();
	}

	public function get_list_bidang() {
		$q=$this->db->select('*')->get('tb_bidang');
		return $q->result();
	}




	/* =================    FUNCTION GET LIST
	=================================================================
	*/

	// Department
	public function get_list_dept() {
		$q=$this->db->select('*')->get('stc_departement');
		return $q->result();
	}

	public function get_data_dept($id) {
		$q=$this->db->select('*')->from('stc_departement')->where('dept_id', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/dept') );
		}
		return $q->row();
	}

	// Position
	public function get_list_position() {
		$q=$this->db->select('*')->get('stc_position');
		return $q->result();
	}

	public function get_data_position($id) {
		$q=$this->db->select('*')->from('stc_position')->where('position_id', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/position') );
		}
		return $q->row();
	}

	// HOD
	public function get_list_hod() {
		$q=$this->db->select('*')->get('stc_hod');
		return $q->result();
	}

	public function get_data_hod($id) {
		$q=$this->db->select('*')->from('stc_hod')->where('hod_id', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/hod') );
		}
		return $q->row();
	}

	// 
	public function get_list_jabatan() {
		$q=$this->db->select('*')->get('tb_jabatan');
		return $q->result();
	}

	public function get_data_jabatan($id) {
		$q=$this->db->select('*')->from('tb_jabatan')->where('id_jabatan', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/jabatan') );
		}
		return $q->row();
	}

	public function get_data_bidang($id) {
		$q=$this->db->select('*')->from('tb_bidang')->where('id_bidang', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/bidang') );
		}
		return $q->row();
	}

	public function get_data_namaizin($id) {
		$q=$this->db->select('*')->from('tb_namaizin')->where('id_namaizin', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/cuti') );
		}
		return $q->row();
	}

	public function get_data_admin($id) {
		$q=$this->db->select('*')->from('tb_admin')->where('id_user', $id)->limit(1)->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/admin') );
		}
		return $q->row();
	}

	public function get_data_pegawai($id) {
		$q=$this->db->select('*')
				->from('tb_pegawai as p')
				->join('tb_bidang as b', 'b.id_bidang = p.id_bidang', 'LEFT')
				->join('tb_jabatan as j', 'j.id_jabatan = p.id_jabatan', 'LEFT')
				->where('id', $id)
				->limit(1)
				->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/pegawai') );
		}
		return $q->row();
	}

	// Employee (Get List)
	public function get_data_employee($id) {
		$q=$this->db->select('*')
				->from('stc_employee as p')
				// ->join('stc_position as b', 'b.position_id = p.position_id', 'LEFT')
				// ->join('stc_dept as j', 'j.dept_id = p.dept_id', 'LEFT')
				->where('emp_id', $id)
				->limit(1)
				->get();
		if( $q->num_rows() < 1 ) {
			redirect( base_url('/data_master/employee') );
		}
		return $q->row();
	}

		// Employee (Get List)
		// resign
		public function get_data_empresign($id) {
			$q=$this->db->select('*')
			->from('view_employee_resign')
			->where('emp_id', $id)
			->limit(1)
			->get();
			if( $q->num_rows() < 1 ) {
				redirect( base_url('/data_master/empresign') );
			}
			return $q->row();
		}
		// finish contract
		public function get_data_empfinish($id) {
			$q=$this->db->select('*')
			->from('view_employee_finish_contract')
			->where('emp_id', $id)
			->limit(1)
			->get();
			if( $q->num_rows() < 1 ) {
				redirect( base_url('/data_master/empfinish') );
			}
			return $q->row();
		}
		// run away
		public function get_data_emprunaway($id) {
			$q=$this->db->select('*')
			->from('view_employee_runaway')
			->where('emp_id', $id)
			->limit(1)
			->get();
			if( $q->num_rows() < 1 ) {
				redirect( base_url('/data_master/emprunaway') );
			}
			return $q->row();
		}

		


	/* =================    FUNCTION EDIT DATA
	=================================================================
	*/
	public function jabatan_update($id,$nama_jabatan) {
		$d_t_d = array(
			'nama_jabatan' => $nama_jabatan
		);
		$this->db->where('id_jabatan', $id)->update('tb_jabatan', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data jabatan berhasil diubah');
	}

	// Position (Edit Data)
	public function position_update($id,$position_name) {
		$d_t_d = array(
			'position_name' => $position_name
		);
		$this->db->where('position_id', $id)->update('stc_position', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data posisi berhasil diubah');
	}

	// Departemen (Edit Data)
	public function dept_update($id,$dept_name) {
		$d_t_d = array(
			'dept_name' => $dept_name
		);
		$this->db->where('dept_id', $id)->update('stc_departement', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data Departemen berhasil diubah');
	}

	// HOD (Edit Data)
	public function hod_update($id,$hod_name) {
		$d_t_d = array(
			'hod_name' => $hod_name
		);
		$this->db->where('hod_id', $id)->update('stc_hod', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data HOD berhasil diubah');
	}


	public function bidang_update($id,$nama_bidang) {
		$d_t_d = array(
			'nama_bidang' => $nama_bidang
		);
		$this->db->where('id_bidang', $id)->update('tb_bidang', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data bidang berhasil diubah');
	}

	public function namaizin_update($id_namaizin,$type,$nama_izin) {
		$d_t_d = array(
			'type' => $type,
			'nama_izin' => $nama_izin
		);
		$this->db->where('id_namaizin', $id_namaizin)->update('tb_namaizin', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data nama izin berhasil diubah');
	}

	public function admin_update($id_user,$username, $email, $namalengkap, $password, $type, $avatar) {
		$d_t_d = array(
			'id_user' => $id_user,
			'username' => $username,
			'email' => $email,
			'namalengkap' => $namalengkap,
			'type' => $type
		);
		if( !empty($password) ) {
			$d_t_d['password'] = md5($password);
		}
		if( !empty($avatar) ) {
			$d_t_d['avatar'] = $avatar;
		}
		$this->db->where('id_user', $id_user)->update('tb_admin', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data admin berhasil diubah');
	}

	// Employee
	public function employee_update(
		$emp_id, $position_id, $emp_badge_number, $emp_name, $emp_date_join, $emp_finish_contract, $emp_phone_number, $emp_whatsapp_number, $emp_work_bpjs_number, 
		$emp_address, $emp_npwp_number, $dept_id, $hod_id, $emp_health_bpjs_number,$emp_email,$emp_bank_rek,$emp_employee_status,$emp_status,$emp_place_birth,
		$emp_date_of_birth,$emp_ktp,$emp_age,$emp_gender,$emp_religion,$emp_hobby,$emp_mariage_status,$emp_urgent_name,$emp_urgent_mobile_number,
		$emp_urgent_address, $emp_medical_by,
		$edu_last,$edu_year,$edu_tittle,$edu_dept,
		$work_year,$work_company,$work_position,$work_sallary,$work_reason,
		$work_year2,$work_company2,$work_position2,$work_sallary2,$work_reason2,
		$emp_img) {
		$d_t_d = array(
			'position_id' => $position_id,
			'emp_badge_number' => $emp_badge_number,
			'emp_name' => $emp_name,
			'emp_date_join' => $emp_date_join,
			'emp_finish_contract' => $emp_finish_contract,
			'emp_phone_number' => $emp_phone_number,
			'emp_whatsapp_number' => $emp_whatsapp_number,
			'emp_work_bpjs_number' => $emp_work_bpjs_number,
			'emp_address' => $emp_address,
			'emp_npwp_number' => $emp_npwp_number,
			'dept_id' => $dept_id, 
			'hod_id' => $hod_id, 
			'emp_health_bpjs_number' => $emp_health_bpjs_number,
			'emp_medical_by' => $emp_medical_by,
			'emp_email' => $emp_email,
			'emp_bank_rek' => $emp_bank_rek,
			'emp_employee_status' => $emp_employee_status,
			'emp_status' => $emp_status,
			'emp_place_birth' => $emp_place_birth,
			'emp_date_of_birth' => $emp_date_of_birth,
			'emp_ktp' => $emp_ktp,
			'emp_age' => $emp_age,
			'emp_gender' => $emp_gender,
			'emp_religion' => $emp_religion,
			'emp_hobby' => $emp_hobby,
			'emp_mariage_status' => $emp_mariage_status,
			'emp_urgent_name' => $emp_urgent_name,
			'emp_urgent_mobile_number' => $emp_urgent_mobile_number,
			'emp_urgent_address' => $emp_urgent_address,

			'edu_last' => $edu_last,
			'edu_year' => $edu_year,
			'edu_tittle' => $edu_tittle,
			'edu_dept' => $edu_dept,

			'work_year' => $work_year,
			'work_company' => $work_company,
			'work_position' => $work_position,
			'work_sallary' => $work_sallary,
			'work_reason' => $work_reason,

			'work_year2' => $work_year2,
			'work_company2' => $work_company2,
			'work_position2' => $work_position2,
			'work_sallary2' => $work_sallary2,
			'work_reason2' => $work_reason2,

		);
		if( !empty($emp_img) ) {
			$d_t_d['emp_img'] = $emp_img;
		}
		$this->db->where('emp_id', $emp_id)->update('stc_employee', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data Karyawan berhasil diubah');
	}

	//
	public function pegawai_update(
		$id,$nama,$nip,$tempat_lahir,$tanggal_lahir,$jenis_kelamin,$pendidikan_terakhir,$status_perkawinan,
		$status_pegawai,$id_jabatan,$id_bidang,$agama,$alamat,$no_ktp,$no_rumah,$no_handphone,$email,
		$password,$id_user,$tanggal_pengangkatan,$avatar
	) {
		$d_t_d = array(
			'nama' => $nama,
			'nip' => $nip,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $jenis_kelamin,
			'pendidikan_terakhir' => $pendidikan_terakhir,
			'status_perkawinan' => $status_perkawinan,
			'status_pegawai' => $status_pegawai,
			'id_jabatan' => $id_jabatan,
			'id_bidang' => $id_bidang,
			'agama' => $agama,
			'alamat' => $alamat,
			'no_ktp' => $no_ktp,
			'no_rumah' => $no_rumah,
			'no_handphone' => $no_handphone,
			'email' => $email,
			'id_user' => $id_user,
			'tanggal_pengangkatan' => $tanggal_pengangkatan
		);
		if( !empty($password) ) {
			$d_t_d['password'] = md5($password);
		}
		if( !empty($avatar) ) {
			$d_t_d['avatar'] = $avatar;
		}
		$this->db->where('id', $id)->update('tb_pegawai', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data pegawai berhasil diubah');
	}




	/* =================    FUNCTION DELETE DATA
	=================================================================
	*/

	public function admin_delete($id) {
		$this->db->delete('tb_admin', array('id_user' => $id));
	}

	public function jabatan_delete($id) {
		$this->db->delete('tb_jabatan', array('id_jabatan' => $id));
	}

	// Posisi (delete)
	public function position_delete($id) {
		$this->db->delete('stc_position', array('position_id' => $id));
	}

	// Departemen (delete)
	public function dept_delete($id) {
		$this->db->delete('stc_departement', array('dept_id' => $id));
	}

	// HOD (delete)
	public function hod_delete($id) {
		$this->db->delete('stc_hod', array('hod_id' => $id));
	}

	public function bidang_delete($id) {
		$this->db->delete('tb_bidang', array('id_bidang' => $id));
	}

	public function pegawai_delete($id) {
		$this->db->delete('tb_pegawai', array('id' => $id));
	}

	public function employee_delete($id) {
		$this->db->delete('stc_employee', array('emp_id' => $id));
	}

	public function namaizin_delete($id) {
		$this->db->delete('tb_namaizin', array('id_namaizin' => $id));
	}




	/* =================    FUNCTION ADD NEW
	=================================================================
	*/	
	public function admin_add_new(
		$username, $email, $namalengkap, $password, $type, $avatar=0
	) {
		$d_t_d = array(
			'username' => $username,
			'email' => $email,
			'namalengkap' => $namalengkap,
			'password' => md5($password),
			'type' => $type,
			'avatar' => $avatar
		);
		if( empty($avatar) ) {
			$d_t_d['avatar'] = 'avatar.png';
		}
		$this->db->insert('tb_admin', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Admin baru berhasil ditambahkan');
	}

	// Employee
	public function employee_add_new(
		$dept_id, $position_id, $emp_badge_number, $emp_name, $emp_date_join, $emp_phone_number, $emp_work_bpjs_number, 
		$emp_address, $emp_npwp_number, $emp_place_birth, $emp_date_of_birth, $emp_ktp, $emp_age, $emp_gender, $emp_religion, $emp_hobby, 
		$emp_mariage_status, $emp_urgent_name, $emp_urgent_mobile_number, $emp_urgent_address,
		$edu_last,$edu_year,$edu_tittle,$edu_dept,
		$work_year,$work_company,$work_position,$work_sallary,$work_reason,
		$work_year2,$work_company2,$work_position2,$work_sallary2,$work_reason2,
		$inf_1,$inf_2,$inf_3,$inf_4,$inf_5,$inf_6, 
		$emp_img=0
	) {
		$d_t_d = array(
			'dept_id' => $dept_id,
			'position_id' => $position_id,
			'emp_badge_number' => $emp_badge_number,
			'emp_name' => $emp_name,
			'emp_date_join' => $emp_date_join,
			'emp_phone_number' => $emp_phone_number,
			'emp_work_bpjs_number' => $emp_work_bpjs_number,
			'emp_address' => $emp_address,
			'emp_npwp_number' => $emp_npwp_number,
			'emp_place_birth' => $emp_place_birth,
			'emp_date_of_birth' => $emp_date_of_birth,
			'emp_ktp' => $emp_ktp,
			'emp_age' => $emp_age,
			'emp_gender' => $emp_gender,
			'emp_religion' => $emp_religion,
			'emp_hobby' => $emp_hobby,
			'emp_mariage_status' => $emp_mariage_status,
			'emp_urgent_name' => $emp_urgent_name,
			'emp_urgent_mobile_number' => $emp_urgent_mobile_number,
			'emp_urgent_address' => $emp_urgent_address,

			'edu_last' => $edu_last,
			'edu_year' => $edu_year,
			'edu_tittle' => $edu_tittle,
			'edu_dept' => $edu_dept,

			'work_year' => $work_year,
			'work_company' => $work_company,
			'work_position' => $work_position,
			'work_sallary' => $work_sallary,
			'work_reason' => $work_reason,

			'work_year2' => $work_year2,
			'work_company2' => $work_company2,
			'work_position2' => $work_position2,
			'work_sallary2' => $work_sallary2,
			'work_reason2' => $work_reason2,

			'inf_1' => $inf_1,
			'inf_2' => $inf_2,
			'inf_3' => $inf_3,
			'inf_4' => $inf_4,
			'inf_5' => $inf_5,
			'inf_6' => $inf_6,

			'emp_img' => $emp_img
		);
		if( empty($emp_img) ) {
			$d_t_d['emp_img'] = 'avatar.png';
		}
		$this->db->insert('stc_employee', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Data Karyawan baru berhasil ditambahkan');
	}

	//
	public function jabatan_add_new(
		$nama_jabatan
	) {
		$d_t_d = array(
			'nama_jabatan' => $nama_jabatan
		);
		$this->db->insert('tb_jabatan', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Jabatan baru berhasil ditambahkan');
	}

	// Position (add New)
	public function position_add_new(
		$position_name
	) {
		$d_t_d = array(
			'position_name' => $position_name
		);
		$this->db->insert('stc_position', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Position baru berhasil ditambahkan');
	}

	// Departemen (add New)
	public function dept_add_new(
		$dept_name
	) {
		$d_t_d = array(
			'dept_name' => $dept_name
		);
		$this->db->insert('stc_departement', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Position baru berhasil ditambahkan');
	}	

	// HOD (add New)
	public function hod_add_new(
		$hod_name
	) {
		$d_t_d = array(
			'hod_name' => $hod_name
		);
		$this->db->insert('stc_hod', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Position baru berhasil ditambahkan');
	}	

	public function bidang_add_new(
		$nama_bidang
	) {
		$d_t_d = array(
			'nama_bidang' => $nama_bidang
		);
		$this->db->insert('tb_bidang', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Bidang baru berhasil ditambahkan');
	}

	public function namaizin_add_new(
		$type,$nama_izin
	) {
		$d_t_d = array(
			'type' => $type,
			'nama_izin' => $nama_izin
		);
		$this->db->insert('tb_namaizin', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Nama izin baru berhasil ditambahkan');
	}

	public function pegawai_add_new(
		$nama,$nip,$tempat_lahir,$tanggal_lahir,$jenis_kelamin,$pendidikan_terakhir,$status_perkawinan,
		$status_pegawai,$id_jabatan,$id_bidang,$agama,$alamat,$no_ktp,$no_rumah,$no_handphone,$email,
		$password,$id_user,$tanggal_pengangkatan,$avatar=0
	) {
		$d_t_d = array(
			'nama' => $nama,
			'nip' => $nip,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $jenis_kelamin,
			'pendidikan_terakhir' => $pendidikan_terakhir,
			'status_perkawinan' => $status_perkawinan,
			'status_pegawai' => $status_pegawai,
			'id_jabatan' => $id_jabatan,
			'id_bidang' => $id_bidang,
			'agama' => $agama,
			'alamat' => $alamat,
			'no_ktp' => $no_ktp,
			'no_rumah' => $no_rumah,
			'no_handphone' => $no_handphone,
			'email' => $email,
			'password' => md5($password),
			'id_user' => $id_user,
			'tanggal_pengangkatan' => $tanggal_pengangkatan,
			'avatar' => $avatar
		);
		if( empty($avatar) ) {
			$d_t_d['avatar'] = 'avatar.png';
		}
		$this->db->insert('tb_pegawai', $d_t_d);
		$this->session->set_flashdata('msg_alert', 'Pegawai baru berhasil ditambahkan');
	}



}

/* End of file M_DataMaster.php */
/* Location: ./application/models/M_DataMaster.php */