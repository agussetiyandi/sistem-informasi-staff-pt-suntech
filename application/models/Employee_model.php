<?php
class Employee_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	public function all()
	{
		// $data=$this->db->query("SELECT * FROM stc_employee WHERE emp_finish_contract < (NOW() - INTERVAL 30 DAY)");
		$data=$this->db->query("SELECT * FROM stc_employee WHERE emp_finish_contract between subdate(curdate(), 30)and curdate();");
		return $data->result();
	}
}