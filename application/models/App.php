<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class App extends CI_Model {
 
   function multi_insert($table = null, $data = array())
   {
      $jumlah = count($data);
 
      if ($jumlah > 0)
      {
         $this->db->insert_batch($table, $data);
      }
   }
 
   function get($table)
   {
      $this->db->from($table);
 
      return $this->db->get();
   }
}