      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper" style="margin-bottom: 0px;">
                <div class="profile-image">
                  <img src="<?=$user_avatar;?>" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name"><?=$user_name;?></p>
                  <div>
                    <small class="designation text-muted">Admin</small>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url('dashboard');?>">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-dm" aria-expanded="false" aria-controls="ui-dm">
              <i class="menu-icon mdi mdi-treasure-chest"></i>
              <span class="menu-title">Manajemen Data Master</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-dm">
              <ul class="nav flex-column sub-menu">
                <?=generate_navlink($path_page, 'data_master/admin', 'Data Admin');?>
                <!-- <?=generate_navlink($path_page, 'data_master/jabatan', 'Data Jabatan');?> -->
                <!-- <?=generate_navlink($path_page, 'data_master/bidang', 'Data Bidang');?> -->
                <!-- <?=generate_navlink($path_page, 'data_master/pegawai', 'Data Pegawai');?> -->
                <!-- <?=generate_navlink($path_page, 'data_master/nama_izin', 'Data Nama Izin');?> -->
                <?=generate_navlink($path_page, 'data_master/hod', 'Data HOD');?>  
                <?=generate_navlink($path_page, 'data_master/dept', 'Data Departemen');?>             
                <?=generate_navlink($path_page, 'data_master/position', 'Data Position');?>
                <!-- <?=generate_navlink($path_page, 'data_master/employee', 'Data Karyawan');?> -->
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url('data_master/employee');?>">
              <i class="menu-icon mdi mdi-treasure-chest"></i>
              <span class="menu-title">Data Karyawan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-dk" aria-expanded="false" aria-controls="ui-dk">
              <i class="menu-icon mdi mdi-treasure-chest"></i>
              <span class="menu-title">Manajemen Karyawan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-dk">
              <ul class="nav flex-column sub-menu">
                <?=generate_navlink($path_page, 'data_master/emprunning', 'Data Running');?>
                <?=generate_navlink($path_page, 'data_master/empfinish', 'Data Finish Contract');?>
                <?=generate_navlink($path_page, 'data_master/empresign', 'Data Resign');?>
                <?=generate_navlink($path_page, 'data_master/emprunaway', 'Data Run Away');?>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=base_url('data_master/renewal');?>">
              <i class="menu-icon mdi mdi-clock"></i>
              <span class="menu-title">Perpanjangan Kontrak</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-md" aria-expanded="false" aria-controls="ui-md">
              <i class="menu-icon mdi mdi-treasure-chest"></i>
              <span class="menu-title">Manajemen Data</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-md">
              <ul class="nav flex-column sub-menu">
                <?=generate_navlink($path_page, 'home/export_excel', 'Export Data');?>
                <?=generate_navlink($path_page, 'excel_import', 'Import Data');?>
              </ul>
            </div>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?=base_url('data_izin');?>">
              <i class="menu-icon mdi mdi-treasure-chest"></i>
              <span class="menu-title">Manajemen Data Izin</span>
            </a>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?=base_url('konfirmasi_izin');?>">
              <i class="menu-icon mdi mdi-checkbox-multiple-marked-outline"></i>
              <span class="menu-title">Konfirmasi Izin</span>
            </a>
          </li> -->
        </ul>
      </nav>
<div class="main-panel">