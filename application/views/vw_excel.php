<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php echo $title ?></title>
  <style>
    ::selection {
      background-color: #E13300;
      color: white;
    }

    ::-moz-selection {
      background-color: #E13300;
      color: white;
    }

    body {
      background-color: #fff;
      margin: auto;
      font: 13px/20px normal Helvetica, Arial, sans-serif;
      color: #4F5155;
    }

    main {
      width: auto;
      padding: 10px;
      background-color: white;
      min-height: 300px;
      border-radius: 5px;
      margin: 30px auto;
      box-shadow: 0 0 8px #D0D0D0;
    }

    table {
      border-top: solid thin #000;
      border-collapse: collapse;
    }

    th,
    td {
      border-top: border-top: solid thin #000;
      padding: 6px 12px;
    }

    a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
    }
  </style>
</head>

<body>
  <main>
    <h2>Preview Data Karyawan</h2>
    <p><a href="<?php echo base_url('home/export_excel') ?>">Export</a></p>
    <table border="1" width="100%">
      <thead>
        <tr>
          <th>No</th>
          <th>Nomor Karyawan</th>
          <th>Nama Karyawan</th>
          <th>Posisi</th>
          <th>Departemen</th>
          <th>HOD</th>
          <th>Tanggal Masuk</th>
          <th>Tanggal Keluar</th>
          <th>No. Rekening BCA</th>
          <th>Jamsostek No.</th>
          <th>NPWP No.</th>
          <th>Agama</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1; foreach($employee as $employee) { ?>
        <tr>
          <td></td>
          <td><?php echo $employee->emp_badge_number; ?></td>
          <td><?php echo $employee->emp_name; ?></td>
          <td><?php echo $employee->position_id; ?></td>
          <td><?php echo $employee->dept_id; ?></td>
          <td><?php echo $employee->hod_id; ?></td>
          <td><?php echo $employee->emp_date_join; ?></td>
          <td><?php echo $employee->emp_finish_contract; ?></td>
          <td><?php echo $employee->emp_bank_rek; ?></td>
          <td><?php echo $employee->emp_work_bpjs_number; ?></td>
          <td><?php echo $employee->emp_npwp_number; ?></td>
          <td><?php echo $employee->emp_religion; ?></td>
        </tr>
        <?php $i++; } ?>
      </tbody>
    </table>
  </main>
</body>

</html>