<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Multi Insert CodeIgniter</title>
 
    <style type="text/css">
 
    ::selection { background-color: #E13300; color: white; }
    ::-moz-selection { background-color: #E13300; color: white; }
 
    body {
        background-color: #fff;
        margin: 40px;
        font: 13px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
    }
 
    h1 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 19px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 45px;
    }
 
    #body {
        margin: 0 15px 0 15px;
    }
 
    p.footer {
        text-align: right;
        font-size: 13px;
        border-top: 1px solid #D0D0D0;
        line-height: 32px;
        padding: 0 40px 0 10px;
        margin: 30px 0 0 0;
    }
 
    #container {
        margin: 10px;
        border: 1px solid #D0D0D0;
        box-shadow: 0 0 8px #D0D0D0;
    }
 
   input[type="text"],
   input[type="number"] {
      padding: 5px 10px 5px 10px;
      margin: 5px;
   }
 
   button[type="submit"] {
      padding: 8px;
      border-radius: 0px;
      background: #005fb6;
      border: 1px solid #1a79d1;
      color: #fefefe;
      cursor: pointer;
   }
 
   button[type="reset"] {
      padding: 8px;
      border-radius: 0px;
      background: #aa3535;
      border: 1px solid #9e3232;
      color: #fefefe;
      cursor: pointer;
   }
    </style>
</head>
<body>
 
<div id="container">
    <h1>Multi Insert CodeIgniter</h1>
 
    <div id="body">
 
      <form action="" method="post">
         <fieldset>
            <legend><b>Tambah Anggota</b></legend>
            <?php for($i=0; $i < 4; $i++) : ?>
               <input type="text" name="education_last[]" value="" placeholder="education_last">
               <input type="text" name="education_last[]" value="" placeholder="education_last">
               <br />
            <?php endfor; ?>
            <br />
            <button type="submit" name="submit" value="submit">Simpan Data</button>
            <button type="reset" name="reset">Reset Data</button>
         </fieldset>
      </form>
 
      <h4>Daftar Anggota</h4>
      <table style="width: 50%" border='1'>
         <thead>
            <tr>
               <th>#</th>
               <th>Nama Anggota</th>
               <th>Telp</th>
            </tr>
         </thead>
 
         <tbody>
            <?php
           if ($data->num_rows() < 1)
           {
              echo '<tr><td colspan="3" align="center">Belum Ada Data</td></tr>';
            }
            else
            {
               $i = 1;
 
               foreach ($data->result() as $key) {
                  echo '<tr>
                           <td align="center">'.$i++.'</td>
                           <td align="center">'.$key->nama_anggota.'</td>
                           <td align="center">'.$key->telp.'</td>
                        </tr>';
               }
            }
            ?>
         </tbody>
      </table>
    </div>
 
</div>
 
</body>
</html>