<html>

<head>
  <title>Application Letter - <?=$data->emp_name;?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
  <style>
    @media print {

      .no-print,
      .no-print * {
        display: none !important
      }
    }

    body,
    table {
      font-family: Arial, Helvetica;
      font-size: 12px !important;
    }

    @font-face {
      font-family: 'Loved by the King';
      font-style: normal;
      font-weight: 400;
      src: url('<?=assets_url('fonts/LovedbytheKing/LovedbytheKing.ttf');?>') format('truetype');
    }

    .main-tittle {
      font-size: 20px;
    }

    .sub-main-tittle {
      font-size: 15px;
    }

    .small-font {
      font-size: 8px;
    }

    .title {
      font-size: 12px;
    }

    .eng {
      font-family: Arial, Helvetica;
      font-size: 10px;
      font-style: italic;
    }

    .signature {
      font-family: 'Loved by the King';
      font-size: 30px;
    }

    .background {
      background-size: cover;
      width: 100%;
      height: auto;
      /* border: solid 2px red; */
    }

    .container {
      position: relative;
      text-align: center;
      color: white;
    }

    .text-left {
      position: absolute;
      bottom: 8px;
      left: 16px;
    }

  </style>
</head>

<body bgcolor="#FFFFFF">
  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" width="85%" height="140">
        <div align="center">
          <img src="<?= base_url('assets/logo.png') ?>"> 
          <br>
          <b class="main-tittle">PT. SUNTECH PLASTICS INDUSTRIES BATAM</b><br>
          Citra Buana Industrial Park III Lot.15 <br>
          Jl. Engku Putri - Batam Centre 29464 <br>
          Telp. 471160 | Fax. 471159
        </div>
        <!-- <hr size="2" align="center" width="100%" color="#000000"/> -->
      </td>
      <td class=small-font width="15%" align="center"> Pass Photo </td>
    </tr>
  </table>
  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>DATA PELAMAR KERJA</b><br>
          <font class="eng"><i>Application Of Employment</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;Posisi &nbsp; &nbsp; &nbsp; :<b> &nbsp; <?=$data->position_id;?> </b></td>
      <td width="50%">&nbsp;No. Karyawan &nbsp; &nbsp; :<b> &nbsp; <?=$data->emp_badge_number;?> </b></td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;Nama &nbsp; &nbsp; &nbsp; :<b> &nbsp; <?=$data->emp_name;?> </b></td>
      <td width="50%">&nbsp;Tanggal Masuk &nbsp; :<b> &nbsp;
          <?=$data->emp_date_join = date_format( date_create($data->emp_date_join), 'd M Y')?> </b></td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;Alamat &nbsp; &nbsp; :<b> &nbsp; <?=$data->emp_address;?> </b></td>
      <td width="50%">&nbsp;No. BPJS Ket &nbsp; &nbsp; :<b> &nbsp; <?=$data->emp_work_bpjs_number;?></td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;No. Telp &nbsp; :<b> &nbsp; <?=$data->emp_phone_number;?> </b></td>
      <td width="50%">&nbsp;No. NPWP &nbsp; &nbsp; &nbsp; &nbsp; :<b> &nbsp; <?=$data->emp_npwp_number;?></td>
    </tr>
    <!-- <tr>
    <td style="text-align: right;">Jakarta, <?=date("d M Y");?></td>
  </tr> -->

    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>DATA DIRI</b><br>
          <font class="eng"><i>Personal Data</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;Tempat, Tgl Lahir &nbsp; :<b> &nbsp; <?=$data->emp_place_birth;?>,
          <?=$data->emp_date_of_birth = date_format( date_create($data->emp_date_of_birth), 'd M Y')?> </b></td>
      <td width="50%">&nbsp;Umur &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        :<b> &nbsp; <?=$data->emp_age;?> th </b></td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;No. KTP &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :<b> &nbsp;
          <?=$data->emp_ktp;?> </b></td>
      <td width="50%">&nbsp;Hobi &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        :<b> &nbsp; <?=$data->emp_hobby;?> </b></td>
    </tr>
    <tr>
      <td width="50%" height="20">&nbsp;Agama &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; :<b> &nbsp;
          <?=$data->emp_religion;?> </b></td>
      <td width="50%">&nbsp;Status Perkawinan &nbsp; :<b> &nbsp; <?=$data->emp_mariage_status;?></td>
    </tr>
  </table>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>PENDIDIKAN</b><br>
          <font class="eng"><i>Education</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><b>Pendidikan Akhir / Kursus</b></td>
      <td align="center" width="auto"><b>Tahun</b></td>
      <td align="center" width="auto"><b>Gelar yang diperoleh</b></td>
      <td align="center" width="auto"><b>Jurusan</b></td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><?=$data->edu_last;?></td>
      <td align="center" width="auto"><?=$data->edu_year;?></td>
      <td align="center" width="auto"><?=$data->edu_tittle;?></td>
      <td align="center" width="auto"><?=$data->edu_dept;?></td>
    </tr>
  </table>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>PENGALAMAN KERJA</b><br>
          <font class="eng"><i>Employment History</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><b>Tahun</b></td>
      <td align="center" width="auto"><b>Nama Perusahaan</b></td>
      <td align="center" width="auto"><b>Jabatan</b></td>
      <td align="center" width="auto"><b>Gaji Pokok/Salary</b></td>
      <td align="center" width="auto"><b>Alasan Berhenti Kerja</b></td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><?=$data->work_year;?></td>
      <td align="center" width="auto"><?=$data->work_company;?></td>
      <td align="center" width="auto"><?=$data->work_position;?></td>
      <td align="center" width="auto"><?=$data->work_sallary;?></td>
      <td align="center" width="20%"><?=$data->work_reason;?></td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><?=$data->work_year2?></td>
      <td align="center" width="auto"><?=$data->work_company2;?></td>
      <td align="center" width="auto"><?=$data->work_position2;?></td>
      <td align="center" width="auto"><?=$data->work_sallary2;?></td>
      <td align="center" width="auto"><?=$data->work_reason2;?></td>
    </tr>
  </table>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>DALAM KEADAAN DARURAT KELUARGA YANG BISA DIHUBUNGI</b><br>
          <font class="eng"><i>In An Emergency Family Can Be Contacted</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><b>Nama</b></td>
      <td align="center" width="auto"><b>Alamat</b></td>
      <td align="center" width="auto"><b>No. Telp</b></td>
    </tr>
    <tr>
      <td align="center" width="auto" height="20"><?=$data->emp_urgent_name;?></td>
      <td align="center" width="auto"><?=$data->emp_urgent_mobile_number;?></td>
      <td align="center" width="auto"><?=$data->emp_urgent_address;?></td>
    </tr>
  </table>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" height="35">
        <div align="center"><b>INFORMASI</b><br>
          <font class="eng"><i>Information</i></font>
        </div>
      </td>
    </tr>
    <tr>
      <td width="80%" height="100">
        &nbsp;1. Apakah anda bisa bekerja pada malam hari ? <br>
        &nbsp;2. Apakah anda siap untuk kerja lembur bila diperintah atasan ? <br>
        &nbsp;3. Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan ? <br>
        &nbsp;4. Apakah anda sudah pernah bekerja di PT. Suntech Plastics sebelumnya ? <br>
        &nbsp;5. Dari siapa anda mendapat informasi lamaran kerja di perusahaan ini ? <br>
        &nbsp;6. Besar gaji yang anda inginkan ? Rp ....
      </td>
      <td align="center" width="20%">
        <?=$data->inf_1;?><br>
        <?=$data->inf_2;?><br>
        <?=$data->inf_3;?><br>
        <?=$data->inf_4;?><br>
        <?=$data->inf_5;?><br>
        <?=$data->inf_6;?><br>
      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <p>&nbsp;Dengan ini saya menyatakan bahwa keterangan yang saya berikan diatas adalah benar dan lengkap sesuai dengan
      lampiran yang <br>
      &nbsp;saya berikan.<br>
      &nbsp;Saya memahami bahwa apabila saya dengan sengaja menyembunyikan atau memalsukan pernyataan pada surat lamaran
      ini,<br>
      &nbsp;maka Perusahaan dapat memberhentikan saya dengan segera dan hubungan kerja saya dengan perusahaan tidak
      berlaku.
    </p>
    <br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td width="50%" align="center">
          <p style="text-align: center;"><br></p>
          <hr size="1" align="center" width="40%" color="#000000">
        </td>
        <td width="50%" align="center">
          <p style="text-align: center;"><br></p>
          <hr size="1" align="center" width="40%" color="#000000">
        </td>
      </tr>
      <tr>
        <td width="45%" align="center">Tanggal/ Date</td>
        <td width="45%" align="center">Tanda Tangan/ Signature of Applicant</td>
      </tr>
    </table>
  </table>

  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

  <!-- <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td colspan="100%" width="100%" height="140">
        <div align="center">
          <b class="main-tittle">PT. SUNTECH PLASTICS INDUSTRIES BATAM</b><br>
          <i class="sub-main-tittle">Employee Orientation Record</i>
        </div>
      </td>
    </tr>
    <td>fkdmf</td>

  </table> -->
  <br>
  <div class="text-left"> </div>
  
  <img align="center" class="container background" src="<?= base_url('assets/printpdf/aplikasi2.jpg') ?>"> </td>
  
  <br>

  <img align="center" class="background" src="<?= base_url('assets/printpdf/aplikasi3.jpg') ?>"> </td>

  <br>

  <img align="center" class="background" src="<?= base_url('assets/printpdf/aplikasi4.jpg') ?>"> </td>

  <tr>
    <?php if(!$dl) { ?>
    <td>
      <div class="no-print" style="margin:0 0 20px;text-align:center;"><a href="javascript:history.go(-1);">&laquo;
          kembali</a> | <a href="javascript:window.print();">print</a></div>
    </td>
    <?php } ?>
  </tr>
  </table>
  <?php if(!$dl) { ?>
  <script type="text/javascript">
    window.print();
  </script>
  <?php } ?>
</body>

</html>