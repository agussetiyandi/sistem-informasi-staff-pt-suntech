<?php

 header("Content-type: application/vnd-ms-excel");
 header("Content-Disposition: attachment; filename=$title.xls");
 header("Pragma: no-cache");
 header("Expires: 0");

 ?>

<table border="1" width="100%">
  <thead>
    <tr>
      <th>No.</th>
      <th>AST</th>
      <th>Nomor Karyawan</th>
      <th>Nama Karyawan</th>
      <th>Rekening</th>
      <th>Posisi</th>
      <th>Departemen</th>
      <th>HOD</th>
      <th>1st Assesment</th>
      <th>Tanggal Masuk</th>
      <th>Tanggal Keluar</th>
      <th>Replace Date</th>
      <th>Badge</th>
      <th>Name</th>
      <th>WL1</th>
      <th>WL2</th>
      <th>WL3</th>
      <th>Akhir Contract 1</th>
      <th>Awal Contract II</th>
      <th>Akhir Contract 2</th>
      <th>Posisi Awal</th>
      <th>No. Rekening</th>
      <th>No. Rekening BCA</th>
      <th>Jamsostek No.</th>
      <th>NPWP No.</th>
      <th>Agama</th>
      <th>Alamat</th>
      <th>No. HP</th>
      <th>No. KTP</th>
      <th>Tanggal Lahir</th>
      <th>Jenis Kelamin</th>
      <th>Status</th>
      <th>BPJS Kes</th>
      <th>Medical By</th>
      <th>Provinsi</th>
      <th>Email</th>
    </tr>
  </thead>

  <tbody>
    <?php $i=1; foreach($employee as $employee) { ?>
      
    <tr>
      <td></td>
      <td></td>
      <td><?php echo $employee->emp_badge_number; ?></td>
      <td><?php echo $employee->emp_name; ?></td>
      <td></td>
      <td><?php echo $employee->position_id; ?></td>
      <td><?php echo $employee->dept_id; ?></td>
      <td><?php echo $employee->hod_id; ?></td>
      <td></td>
      <td><?php echo $employee->emp_date_join; ?></td>
      <td><?php echo $employee->emp_finish_contract; ?></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td><?php echo $employee->emp_bank_rek; ?></td>
      <td><?php echo $employee->emp_work_bpjs_number; ?></td>
      <td><?php echo $employee->emp_npwp_number; ?></td>
      <td><?php echo $employee->emp_religion; ?></td>
      <td><?php echo $employee->emp_address; ?></td>
      <td><?php echo $employee->emp_phone_number; ?></td>
      <td><?php echo $employee->emp_ktp; ?></td>
      <td><?php echo $employee->emp_date_of_birth; ?></td>
      <td><?php echo $employee->emp_gender; ?></td>
      <td><?php echo $employee->emp_status; ?></td>
      <td><?php echo $employee->emp_health_bpjs_number; ?></td>
      <td><?php echo $employee->emp_medical_by; ?></td>
      <td></td>
      <td><?php echo $employee->emp_email; ?></td>
    </tr>

    <?php $i++; } ?>

  </tbody>
</table>