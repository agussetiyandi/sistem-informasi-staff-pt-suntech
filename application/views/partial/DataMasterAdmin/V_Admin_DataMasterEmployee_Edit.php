<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <!-- <h4 class="card-title"><?=$title_page;?></h4> -->
          <?php if($this->session->flashdata('msg_alert')) { ?>

          <div class="alert alert-info">
            <label style="font-size: 13px;"><?=$this->session->flashdata('msg_alert');?></label>
          </div>
          <?php } ?>
          <?=form_open_multipart('data_master/edit/employee/' . $data_employee->emp_id , array('method'=>'post'));?>
          <input type="hidden" name="emp_id" value="<?=$data_employee->emp_id;?>">

          <h4>DATA PELAMAR KERJA</h4>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_name">Nama Lengkap</label>
                <input type="text" value="<?=$data_employee->emp_name;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_badge_number">No. Karyawan</label>
                <input type="text" value="<?=$data_employee->emp_badge_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_badge_number">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_date_join">Tanggal Masuk</label>
                <input type="date" value="<?=$data_employee->emp_date_join;?>" class="form-control" name="emp_date_join">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_finish_contract">Kontrak Berakhir</label>
                <input type="date" value="<?=$data_employee->emp_finish_contract;?>" class="form-control" name="emp_finish_contract">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="position_id">Posisi</label>
                <select name="position_id" class="form-control">
                  <option disabled selected>-- Pilih Posisi --</option>
                  <?php
                    foreach($list_position as $lp) {
                  ?>
                  <option value="<?=$lp->position_name;?>"
                    <?=( ($data_employee->position_id==$lp->position_name) ? 'selected' : '');?>>
                    <?=$lp->position_name;?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="dept_id">Departemen</label>
                <select name="dept_id" class="form-control">
                  <option disabled selected>-- Pilih Departemen --</option>
                  <?php
                    foreach($list_dept as $ld) {
                  ?>
                  <option value="<?=$ld->dept_name;?>"
                    <?=( ($data_employee->dept_id==$ld->dept_name) ? 'selected' : '');?>><?=$ld->dept_name;?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="hod_id">HOD</label>
                <select name="hod_id" class="form-control">
                  <option disabled selected>-- Pilih HOD --</option>
                  <?php
                    foreach($list_hod as $lh) {
                  ?>
                  <option value="<?=$lh->hod_id;?>" <?=( ($data_employee->hod_id==$lh->hod_id) ? 'selected' : '');?>>
                    <?=$lh->hod_name;?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_work_bpjs_number">No. BPJS Ketenagakerjaan</label>
                <input type="text" value="<?=$data_employee->emp_work_bpjs_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_work_bpjs_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_health_bpjs_number">No. BPJS Kesehatan</label>
                <input type="text" value="<?=$data_employee->emp_health_bpjs_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_health_bpjs_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_npwp_number">Medical By</label>
                <input type="text" value="<?=$data_employee->emp_medical_by;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_medical_by">
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_phone_number">No. Telp</label>
                <input type="text" value="<?=$data_employee->emp_phone_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_phone_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_whatsapp_number">No. Whatsapp</label>
                <input type="text" value="<?=$data_employee->emp_whatsapp_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_whatsapp_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_email">Email</label>
                <input type="text" value="<?=$data_employee->emp_email;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_npwp_number">No. NPWP</label>
                <input type="text" value="<?=$data_employee->emp_npwp_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_npwp_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_bank_rek">No. Rekening Bank</label>
                <input type="text" value="<?=$data_employee->emp_bank_rek;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_bank_rek">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_address">Alamat</label>
                <input type="text" value="<?=$data_employee->emp_address;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_address">
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_employee_status">Status Kontrak</label>
                <select name="emp_employee_status" class="form-control">
                  <option disabled selected>-- Pilih Status--</option>
                  <option value="KONTRAK" <?=( ($data_employee->emp_employee_status=='KONTRAK') ? 'selected' : '');?>>
                    KONTRAK</option>
                  <option value="PERMANEN" <?=( ($data_employee->emp_employee_status=='PERMANEN') ? 'selected' : '');?>>
                    PERMANEN</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_status">Status Karyawan</label>
                <select name="emp_status" class="form-control">
                  <option disabled selected>-- Pilih Status--</option>
                  <option value="RUNNING" <?=( ($data_employee->emp_status=='RUNNING') ? 'selected' : '');?>>RUNNING
                  </option>
                  <option value="FINISH CONTRACT"
                    <?=( ($data_employee->emp_status=='FINISH CONTRACT') ? 'selected' : '');?>>FINISH CONTRACT</option>
                  <option value="RESIGN" <?=( ($data_employee->emp_status=='RESIGN') ? 'selected' : '');?>>RESIGN
                  </option>
                  <option value="RUN AWAY" <?=( ($data_employee->emp_status=='RUN AWAY') ? 'selected' : '');?>>RUN AWAY
                  </option>
                </select>
              </div>
            </div>
          </div>
          <br>

          <h4>DATA DIRI / PERSONAL</h4>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_place_birth">Tempat lahir</label>
                <input type="text" value="<?=$data_employee->emp_place_birth;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_place_birth">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_date_of_birth">Tanggal Lahir</label>
                <input type="date" value="<?=$data_employee->emp_date_of_birth;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_date_of_birth">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_ktp">No. KTP</label>
                <input type="text" value="<?=$data_employee->emp_ktp;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_ktp">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_age">Umur</label>
                <input type="text" value="<?=$data_employee->emp_age;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_age">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_gender">Jenis Kelamin</label>
                <select name="emp_gender" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="LAKI-LAKI" <?=( ($data_employee->emp_gender=='LAKI-LAKI') ? 'selected' : '');?>>
                    LAKI-LAKI</option>
                  <option value="PEREMPUAN" <?=( ($data_employee->emp_gender=='Perempuan') ? 'selected' : '');?>>
                    PEREMPUAN</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_religion">Agama</label>
                <select name="emp_religion" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="ISLAM" <?=( ($data_employee->emp_religion=='ISLAM') ? 'selected' : '');?>>ISLAM
                  </option>
                  <option value="PROTESTAN" <?=( ($data_employee->emp_religion=='PROTESTAN') ? 'selected' : '');?>>
                    PROTESTAN</option>
                  <option value="KATOLIK" <?=( ($data_employee->emp_religion=='KATOLIK') ? 'selected' : '');?>>KATOLIK
                  </option>
                  <option value="HINDU" <?=( ($data_employee->emp_religion=='HINDU') ? 'selected' : '');?>>HINDU
                  </option>
                  <option value="BUDDHA" <?=( ($data_employee->emp_religion=='BUDDHA') ? 'selected' : '');?>>BUDDHA
                  </option>
                  <option value="KHONGHUCU" <?=( ($data_employee->emp_religion=='KHONGHUCU') ? 'selected' : '');?>>
                    KHONGHUCU</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_hobby">Hobi</label>
                <input type="text" value="<?=$data_employee->emp_hobby;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_hobby">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="emp_mariage_status">Status Perkawinan</label>
                <select name="emp_mariage_status" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="SINGLE" <?=( ($data_employee->emp_mariage_status=='SINGLE') ? 'selected' : '');?>>
                    SINGLE</option>
                  <option value="MENIKAH" <?=( ($data_employee->emp_mariage_status=='MENIKAH') ? 'selected' : '');?>>
                    MENIKAH</option>
                </select>
              </div>
            </div>
          </div>
          <br>

          <h4>PENDIDIKAN / EDUCATION</h4>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_last">Pendidikan Akhir/ Kursus</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->edu_last;?>" name="edu_last" placeholder="Pendidikan Akhir/ Kursus">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_year">Tahun / Year</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->edu_year;?>" name="edu_year" placeholder="Tahun / Year">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_tittle">Gelar yang diperoleh</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->edu_tittle;?>" name="edu_tittle" placeholder="Gelar yang diperoleh">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_dept">Jurusan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->edu_dept;?>" name="edu_dept" placeholder="Jurusan">
              </div>
            </div>
          </div>
          <br>

          <h4>PENGALAMAN KERJA / EMPLOYMENT HISTORY</h4>
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <label for="work_year">Tahun</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_year;?>" name="work_year"
                  placeholder="Tahun">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="work_company">Nama Perusahaan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_company;?>" name="work_company" placeholder="Nama Perusahaan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="work_position">Jabatan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_position;?>" name="work_position" placeholder="Jabatan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="work_sallary">Gaji Pokok/Sallary</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_sallary;?>" name="work_sallary" placeholder="Gaji Pokok/Sallary">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="work_reason">Alasan berhenti bekerja</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_reason;?>" name="work_reason" placeholder="Alasan berhenti bekerja">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_year2;?>" name="work_year2" placeholder="Tahun">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_company2;?>" name="work_company2" placeholder="Nama Perusahaan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_position2;?>" name="work_position2" placeholder="Jabatan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_sallary2;?>" name="work_sallary2" placeholder="Gaji Pokok/Sallary">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?=$data_employee->work_reason2;?>" name="work_reason2" placeholder="Alasan berhenti bekerja">
              </div>
            </div>
          </div>
          <br>

          <h4>KELUARGA YANG BISA DIHUBUNGI DALAM KEADAAN DARURAT</h4>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_urgent_name">Nama</label>
                <input type="text" value="<?=$data_employee->emp_urgent_name;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_urgent_name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_urgent_mobile_number">No. HP</label>
                <input type="text" value="<?=$data_employee->emp_urgent_mobile_number;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_urgent_mobile_number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_urgent_address">Alamat</label>
                <input type="text" value="<?=$data_employee->emp_urgent_address;?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_urgent_address">
              </div>
            </div>
          </div>
          <br>

          <!-- <h4>INFORMASI / INFORMATION</h4>

          <body>
            <p>1. Apakah anda bisa bekerja pada malam hari?</p>
            <p>2. Apakah anda siap untuk kerja lembur bila diperintah atasan?</p>
            <p>3. Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?</p>
            <p>4. Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?</p>
            <p>5. Apakah anda sudah pernah bekerja di PT. Suntech Plastics sebelumnya? Tahun . . . .</p>
            <p>6. Besar gaji yang anda inginkan? Rp . . . .</p>
          </body> -->


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <button type="submit" class="btn btn-success mr-2">Update</button>
                <button class="btn btn-light" type="reset">Reset</button>
              </div>
            </div>
          </div>
          <?=form_close();?>
        </div>
      </div>
    </div>
  </div>
</div>
