<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?=$title_page;?></h4>
          <?php if($this->session->flashdata('msg_alert')) { ?>

          <div class="alert alert-info">
            <label style="font-size: 13px;"><?=$this->session->flashdata('msg_alert');?></label>
          </div>
          <?php } ?>
          <?=form_open_multipart('data_master/add_new/employee', array('method'=>'post'));?>

          <h4>DATA PELAMAR KERJA</h4>
          <!-- <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Departemen</label>
                <select class="form-control" name="stc_departement" id="stc_departement" required>
                  <option disabled selected>-- Pilih --</option>
                  <?php foreach($departement as $row):?>
                  <option value="<?php echo $row->dept_id;?>"><?php echo $row->dept_name;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Posisi</label>
                <select class="form-control" id="stc_position" name="stc_position" required>
                  <option disabled selected>-- Pilih --</option>
                </select>
              </div>
            </div>
          </div> -->


          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="dept_id">Departemen</label>
                <select name="dept_id" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <?php
                      foreach($list_dept as $ld) {
                    ?>
                  <option value="<?=$ld->dept_name;?>"><?=$ld->dept_name;?></option>
                  <?php
                      }
                    ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="position_id">Posisi</label>
                <select name="position_id" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <?php
                      foreach($list_position as $lp) {
                    ?>
                  <option value="<?=$lp->position_name;?>"><?=$lp->position_name;?></option>
                  <?php
                      }
                    ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="emp_badge_number">No. Karyawan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="emp_badge_number" class="form-control"
                  placeholder=". . . ." />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Lengkap</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_name" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tanggal Masuk</label>
                <div class="col-sm-9">
                  <input type="date" name="emp_date_join" class="form-control" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">No. Telp</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_phone_number" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">No. BPJS Ketenagakerjaan</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_work_bpjs_number" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                  <textarea name="emp_address" class="form-control" onkeyup="this.value = this.value.toUpperCase()" rows="3" placeholder=". . ." /></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">No. NPWP</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_npwp_number" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <br>

          <h4>DATA DIRI / PERSONAL</h4>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tempat Lahir</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_place_birth" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                <div class="col-sm-9">
                  <input type="date" name="emp_date_of_birth" class="form-control" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">No. KTP</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_ktp" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Umur</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_age" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-9">
                  <select name="emp_gender" class="form-control">
                    <option disabled selected>-- Pilih --</option>
                    <option value="LAKI-LAKI">LAKI-LAKI</option>
                    <option value="PEREMPUAN">PEREMPUAN</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-9">
                  <select name="emp_religion" class="form-control">
                    <option disabled selected>-- Pilih --</option>
                    <option value="ISLAM">ISLAM</option>
                    <option value="PROTESTAN">PROTESTAN</option>
                    <option value="KATOLIK">KATOLIK</option>
                    <option value="HINDU">HINDU</option>
                    <option value="BUDDHA">BUDDHA</option>
                    <option value="KHONGHUCU">KHONGHUCU</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Hobi</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_hobby" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status Perkawinan</label>
                <div class="col-sm-9">
                  <select name="emp_mariage_status" class="form-control">
                    <option disabled selected>-- Pilih --</option>
                    <option value="SINGLE">SINGLE</option>
                    <option value="MENIKAH">MENIKAH</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <br>

          <h4>PENDIDIKAN / EDUCATION</h4>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_last">Pendidikan Akhir/ Kursus</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="edu_last" placeholder="Pendidikan Akhir/ Kursus">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_year">Tahun / Year</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="edu_year" placeholder="Tahun / Year">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_tittle">Gelar yang diperoleh</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="edu_tittle" placeholder="Gelar yang diperoleh">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="edu_dept">Jurusan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="edu_dept" placeholder="Jurusan">
              </div>
            </div>
          </div>
          <br>

          <h4>PENGALAMAN KERJA / EMPLOYMENT HISTORY</h4>
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <label for="work_year">Tahun</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_year" placeholder="Tahun">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="work_company">Nama Perusahaan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_company" placeholder="Nama Perusahaan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="work_position">Jabatan</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_position" placeholder="Jabatan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="work_sallary">Gaji Pokok/Sallary</label>
                <input type="currency" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_sallary" placeholder="Gaji Pokok/Sallary">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="work_reason">Alasan berhenti bekerja</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_reason" placeholder="Alasan berhenti bekerja">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_year2" placeholder="Tahun">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_company2" placeholder="Nama Perusahaan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_position2" placeholder="Jabatan">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="currency" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_sallary2" placeholder="Gaji Pokok/Sallary">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="work_reason2" placeholder="Alasan berhenti bekerja">
              </div>
            </div>
          </div>
          <br>

          <h4>KELUARGA YANG BISA DIHUBUNGI DALAM KEADAAN DARURAT</h4>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_urgent_name" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">No. HP</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_urgent_mobile_number" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                  <input type="text" name="emp_urgent_address" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . ." />
                </div>
              </div>
            </div>
          </div>
          <br>

          <h4>INFORMASI / INFORMATION</h4>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_1">Apakah anda bisa bekerja pada malam hari?</label>
                <select name="inf_1" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="YA"> YA</option>
                  <option value="TIDAK">TIDAK</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_2">Apakah anda siap untuk kerja lembur bila diperintah atasan?</label>
                <select name="inf_2" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="YA"> YA</option>
                  <option value="TIDAK">TIDAK</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_3">Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?</label>
                <select name="inf_3" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="YA"> YA</option>
                  <option value="TIDAK">TIDAK</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_4">Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?</label>
                <select name="inf_4" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="YA"> YA</option>
                  <option value="TIDAK">TIDAK</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_5">Apakah anda sudah pernah bekerja di PT. Suntech Plastics sebelumnya?</label>
                <select name="inf_5" class="form-control">
                  <option disabled selected>-- Pilih --</option>
                  <option value="YA"> YA</option>
                  <option value="TIDAK">TIDAK</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="inf_6">Besar gaji yang anda inginkan? Rp . . . .</label>
                <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" name="inf_6" placeholder=". . . .">
              </div>
            </div>
          </div>

          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <button class="btn btn-light" type="reset">Reset</button>
              </div>
            </div>
          </div>
          <?=form_close();?>
        </div>
      </div>
    </div>
  </div>
</div>