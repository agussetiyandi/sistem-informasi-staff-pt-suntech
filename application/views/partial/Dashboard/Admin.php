<div class="content-wrapper">
  <div class="row">

  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account-location text-info icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Data Karyawan</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=$total_employee;?></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-account-location mr-1" aria-hidden="true"></i> Jumlah Karyawan
          </p>
        </div>
      </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-treasure-chest text-warning icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Data Renewal Kontrak</p>
              <div class="fluid-container">
                <!-- <h3 class="font-weight-medium text-right mb-0"><?=$total_dataizin;?></h3> -->
                <h3 class="font-weight-medium text-right mb-0"><?=$total_datarenewal;?></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-treasure-chest mr-1" aria-hidden="true"></i> Jumlah Data Kontrak Berakhir
          </p>
        </div>
      </div>
    </div>

    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account-star text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Admin</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=$total_admin;?></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-account-star mr-1" aria-hidden="true"></i> Jumlah Data Admin
          </p>
        </div>
      </div>
    </div>

    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-checkbox-multiple-marked-outline text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Izin Terkonfirmasi</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=$total_izinterkonfirmasi;?></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-checkbox-multiple-marked-outline mr-1" aria-hidden="true"></i> Jumlah izin terkonfirmasi
          </p>
        </div>
      </div>
    </div> -->




  </div>
</div>