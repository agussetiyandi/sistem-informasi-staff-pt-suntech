<!DOCTYPE html>
<html>
<head>
  <title>Codeigniter Export Example</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>
     
    <div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
                <th class="header">Posisi</th>
                <th class="header">Departemen</th>                           
                <th class="header">No. Karyawan</th>                      
                <th class="header">Nama</th>
                <th class="header">KTP</th>
            </tr>
        </thead>
        <a class="pull-right btn btn-primary btn-xs" href="export/createxls"><i class="fa fa-file-excel-o"></i> Export Data</a>
        <tbody>
            <?php
            if (isset($export_list) && !empty($export_list)) {
                foreach ($export_list as $key => $list) {
                    ?>
                    <tr>
                        <td><?php echo $list->position_id; ?></td>   
                        <td><?php echo $list->dept_id; ?></td> 
                        <td><?php echo $list->emp_badge_number; ?></td>                       
                        <td><?php echo $list->emp_name; ?></td>
                        <td><?php echo $list->emp_ktp; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">There is no employee.</td>    
                </tr>
            <?php } ?>
 
        </tbody>
    </table>
    </div> 
 
</body>
</html>