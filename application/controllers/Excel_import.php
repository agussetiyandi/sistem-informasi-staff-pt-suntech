<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_import extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
	}

	public function index() {
		$data['num_rows'] = $this->db->get('stc_employee')->num_rows();

		$this->load->view('excel_import', $data);
	}

	public function import_data() {
		$config = array(
			'upload_path'   => FCPATH.'uploads/excel/',
			'allowed_types' => 'xls|csv'
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
			@chmod($data['full_path'], 0777);

			$this->load->library('Spreadsheet_Excel_Reader');
			$this->spreadsheet_excel_reader->setOutputEncoding('CP1251');

			$this->spreadsheet_excel_reader->read($data['full_path']);
			$sheets = $this->spreadsheet_excel_reader->sheets[0];
			error_reporting(0);

			$data_excel = array();
			for ($i = 2; $i <= $sheets['numRows']; $i++) {
				if ($sheets['cells'][$i][1] == '') break;

				$data_excel[$i - 1]['emp_badge_number']    = $sheets['cells'][$i][1];
				$data_excel[$i - 1]['emp_name']   = $sheets['cells'][$i][2];
				$data_excel[$i - 1]['emp1_rek'] = $sheets['cells'][$i][3];
				$data_excel[$i - 1]['position_id']    = $sheets['cells'][$i][4];
				$data_excel[$i - 1]['dept_id']   = $sheets['cells'][$i][5];
				$data_excel[$i - 1]['hod_id'] = $sheets['cells'][$i][6];
				$data_excel[$i - 1]['emp1_1st_ass']    = $sheets['cells'][$i][7];
				$data_excel[$i - 1]['emp_date_join']   = $sheets['cells'][$i][8];
				$data_excel[$i - 1]['emp1_date_out'] = $sheets['cells'][$i][9];
				$data_excel[$i - 1]['emp1_replace_date']    = $sheets['cells'][$i][10];
				$data_excel[$i - 1]['emp1_badge']   = $sheets['cells'][$i][11];
				$data_excel[$i - 1]['emp1_wl1'] = $sheets['cells'][$i][12];
				$data_excel[$i - 1]['emp1_wl2'] = $sheets['cells'][$i][13];
				$data_excel[$i - 1]['emp1_wl3'] = $sheets['cells'][$i][14];
				$data_excel[$i - 1]['emp_finish_contract'] = $sheets['cells'][$i][15];
				$data_excel[$i - 1]['emp1_start_contract2']   = $sheets['cells'][$i][16];
				$data_excel[$i - 1]['emp1_finish_contract2'] = $sheets['cells'][$i][17];
				$data_excel[$i - 1]['emp1_first_position']    = $sheets['cells'][$i][18];
				$data_excel[$i - 1]['emp1_no_rek']   = $sheets['cells'][$i][19];
				$data_excel[$i - 1]['emp_bank_rek'] = $sheets['cells'][$i][20];
				$data_excel[$i - 1]['emp_work_bpjs_number']    = $sheets['cells'][$i][21];
				$data_excel[$i - 1]['emp_npwp_number']   = $sheets['cells'][$i][22];
				$data_excel[$i - 1]['emp_religion'] = $sheets['cells'][$i][23];
				$data_excel[$i - 1]['emp_address']    = $sheets['cells'][$i][24];
				$data_excel[$i - 1]['emp_phone_number']   = $sheets['cells'][$i][25];
				$data_excel[$i - 1]['emp_ktp'] = $sheets['cells'][$i][26];
				$data_excel[$i - 1]['emp_place_birth'] = $sheets['cells'][$i][27];
				$data_excel[$i - 1]['emp_date_of_birth'] = $sheets['cells'][$i][28];
				$data_excel[$i - 1]['emp_gender'] = $sheets['cells'][$i][29];
				$data_excel[$i - 1]['emp_mariage_status']    = $sheets['cells'][$i][30];
				$data_excel[$i - 1]['emp_health_bpjs_number']   = $sheets['cells'][$i][31];
				$data_excel[$i - 1]['emp_medical_by'] = $sheets['cells'][$i][32];
				$data_excel[$i - 1]['emp_email'] = $sheets['cells'][$i][33];
				$data_excel[$i - 1]['emp_email'] = $sheets['cells'][$i][34];
			}

			$this->db->insert_batch('stc_employee', $data_excel);
			// @unlink($data['full_path']);
			redirect('excel_import');
		}
	}

}

/* End of file Excel_import.php */
/* Location: ./application/controllers/Excel_import.php */