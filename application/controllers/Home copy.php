<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  public function __construct() {
    parent::__construct();

    $this->load->model('Model');
  }

  public function index() {
    $data['title'] = 'preview data';

    $data['datas'] = $this->Model->getAll()->result_array();
    $data['fields'] = $this->Model->getFields();
    $data['numCol'] = $this->Model->getCol();

    $this->load->view('header', $data);
    $this->load->view('preview', $data);
  }

  public function new_data() {
    $data['title'] = 'insert data';

    if( $this->input->post('emp_badge_number') ) {
      $data = [
        'emp_badge_number' => $this->input->post('emp_badge_number'),
        'emp_name' => $this->input->post('emp_name'),
        'position_id' => $this->input->post('position_id'),
        'dept_id' => $this->input->post('dept_id'),
        'emp_phone_number' => $this->input->post('emp_phone_number'),
        'emp_email' => $this->input->post('emp_email')
      ];
  
      $this->Model->post($data);
      $this->session->set_flashdata('flash', 'New data has been added');
      redirect('');
    }

    $this->load->view('header', $data);
    $this->load->view('form', $data);
  }

  public function up_data($id) {
    $data['title'] = 'insert data';

    if( $this->input->post('emp_badge_number') ) {
      $data = [
        'emp_badge_number' => $this->input->post('emp_badge_number'),
        'emp_name' => $this->input->post('emp_name'),
        'position_id' => $this->input->post('position_id'),
        'dept_id' => $this->input->post('dept_id'),
        'emp_phone_number' => $this->input->post('emp_phone_number'),
        'emp_email' => $this->input->post('emp_email')
      ];
  
      $this->Model->update($id, $data);
      $this->session->set_flashdata('flash', 'Selected data has been updated');
      redirect('');
    }
    $where = [
      'emp_id' => $id
    ];

    $data['data'] = $this->Model->get($where)->row();

    $this->load->view('header', $data);
    $this->load->view('form', $data);
  }

  public function del_data($id) {
    $this->Model->delete($id);
    $this->session->set_flashdata('flash', 'Selected data successfully deleted');
    redirect('');
  }

}

