<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_Application_Letter extends CI_Controller {
	
	private $m_ai;

	function __construct() {
		parent::__construct();
		$this->load->model('M_DataMaster');
		$this->m_datamaster = $this->M_DataMaster;
	}

	public function index()
	{
		isnt_login(function() {
			redirect( base_url('auth/login') );
		});
		redirect( base_url('dashboard') );
	}

	public function print()
	{
		if( empty($this->uri->segment('3'))) {
			redirect( base_url('/dashboard') );
		}

		$id=$this->uri->segment('3');
		$data['dl'] = false;
		$data['emp_id'] = $id;
		$data['user_name'] = $this->session->userdata('user_name');
		$data['data'] = $this->m_datamaster->get_data_employee($id);

		$data['emp_name'] = $data['data']->emp_name;
		// $data['type'] = $data['data']->type;
		// switch ($data['type']) {
		// 	case 'cuti':
		// 			$data['type_id'] = '001';
		// 		break;
		// 	case 'sekolah':
		// 			$data['type_id'] = '002';
		// 		break;
		// 	case 'seminar':
		// 			$data['type_id'] = '003';
		// 		break;
		// }
		// if( $_SERVER['REQUEST_METHOD'] == 'GET') {
		// 	if( isset($_GET['dl']) ) {
		// 		$data['dl'] = true;
		// 		header("Content-type: application/vnd.ms-word");
		// 		header("Content-Disposition: attachment; filename=SkBAAK-{$data['type_id']}-{$id}.doc");
		// 	}
		// }
		$data['position_id'] = strtoupper($data['data']->position_id);
		$data['emp_badge_number'] = strtoupper($data['data']->emp_badge_number);
		$data['emp_name'] = strtoupper($data['data']->emp_name);
		$data['emp_date_join'] = date_format( date_create($data['data']->emp_date_join), 'd M Y');
		$data['emp_address'] = $data['data']->emp_address;
		$data['emp_work_bpjs_number'] = strtoupper($data['data']->emp_work_bpjs_number);
		$data['emp_phone_number'] = strtoupper($data['data']->emp_phone_number);
		$data['emp_npwp_number'] = strtoupper($data['data']->emp_npwp_number);

		// $data['nama'] = explode(' ', $data['data']->nama)[0];
		// $diff  = date_diff( date_create($data['data']->tglawal), date_create($data['data']->tglakhir) );
		// $data['selama'] = $diff->format('%d hari');
		// $data['tglawal'] = date_format( date_create($data['data']->tglawal), 'd M Y');
		// $data['tglakhir'] = date_format( date_create($data['data']->tglakhir), 'd M Y');
		$this->load->view('Application_Letter_Print', $data);
	}
}

/* End of file Surat_Keterangan.php */
/* Location: ./application/controllers/Surat_Keterangan.php */