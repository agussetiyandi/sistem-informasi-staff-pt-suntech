<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Master extends CI_Controller {

	private $m_datamaster;

	function __construct() {
		parent::__construct();
		isnt_admin(function() {
			redirect( base_url('auth/login') );
		});
		$this->load->model('M_DataMaster');
		$this->m_datamaster = $this->M_DataMaster;

		$this->load->helper("url");
		$this->load->model('employee_model');
	}

	public function index() {
		redirect( base_url('dashboard') );

		$data['content'] = $this->employee_model->all();
		$this->load->view('partial/DataMasterAdmin/v_admin_datamasteremployeerenewal_read', $data);
	}



	/* =================    FUNCTION GET DATA
	=================================================================
	*/

	public function admin() {
		$data = generate_page('Data Master Admin', 'data_master/admin', 'Admin');

			$data_content['title_page'] = 'Data Master Admin';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterAdmin_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function admin_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->admin_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$value->avatar = '<img src="' . uploads_url('avatar/'.$value->avatar) . '" alt="image" />';
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

	public function jabatan() {
		$data = generate_page('Data Master Jabatan', 'data_master/jabatan', 'Admin');

			$data_content['title_page'] = 'Data Master Jabatan';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterJabatan_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function jabatan_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->jabatan_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

	// Departement
	public function dept() {
		$data = generate_page('Data Master Departement', 'data_master/dept', 'Admin');

			$data_content['title_page'] = 'Data Master Departement';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterDept_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function dept_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->dept_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

		// Position
		public function position() {
			$data = generate_page('Data Master Posisi', 'data_master/position', 'Admin');
	
				$data_content['title_page'] = 'Data Master Posisi';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPosition_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function position_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->position_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$i++;
				}
				return array('data' => $new_arr);
			});
		}

	// Head of Department
	public function hod() {
		$data = generate_page('Data Master HOD', 'data_master/hod', 'Admin');

			$data_content['title_page'] = 'Data Master HOD';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterHod_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function hod_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->hod_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

	// Bidang
	public function bidang() {
		$data = generate_page('Data Master Bidang', 'data_master/bidang', 'Admin');

			$data_content['title_page'] = 'Data Master Bidang';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterBidang_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
		
	}

	public function bidang_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->bidang_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

	// Employee
	public function employee() {
		$data = generate_page('Data Master Karyawan', 'data_master/employee', 'Admin');

			$data_content['title_page'] = 'Data Master Karyawan';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterEmployee_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function employee_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->employee_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd/m/Y');
				$value->emp_img = '<img src="' . uploads_url('avatar/'.$value->emp_img) . '" alt="image" />';
				$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd/m/Y');
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

		// Renewal Kontrak
		public function renewal() {
			$data = generate_page('Data Renewal Kontrak', 'data_master/renewal', 'Admin');
	
			$data_content['title_page'] = 'Data Master Renewal';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterRenewal_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function renewal_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->renewal_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd M Y');
					$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd M Y');
					$i++;
				}
				return array('data' => $new_arr);
			});
		}

		// Employee (Get Data) 
		// (resign)
		public function empresign() {
			$data = generate_page('Data Karyawan Resign', 'data_master/empresign', 'Admin');
	
			$data_content['title_page'] = 'Data Karyawan Resign';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterEmpResign_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function empresign_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->empresign_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd M Y');
					$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd M Y');
					$i++;
				}
				return array('data' => $new_arr);
			});
		}

		// (finish contract)
		public function empfinish() {
			$data = generate_page('Data Karyawan Finish Contract', 'data_master/empfinish', 'Admin');
	
			$data_content['title_page'] = 'Data Karyawan Finish Contract';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterFinishContract_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function empfinish_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->empfinish_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd M Y');
					$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd M Y');
					$i++;
				}
				return array('data' => $new_arr);
			});
		}

		// (run away)
		public function emprunaway() {
			$data = generate_page('Data Karyawan Run Away', 'data_master/emprunaway', 'Admin');
	
			$data_content['title_page'] = 'Data Karyawan Run Away';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterRunaway_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function emprunaway_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->emprunaway_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd M Y');
					$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd M Y');
					$i++;
				}
				return array('data' => $new_arr);
			});
		}

		// (running)
		public function emprunning() {
			$data = generate_page('Data Karyawan Running', 'data_master/emprunning', 'Admin');
	
			$data_content['title_page'] = 'Data Karyawan Running';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterRunning_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}
	
		public function emprunning_ajax() {
			json_dump(function() {
				$result= $this->m_datamaster->emprunning_list_all();
				$new_arr=array();$i=1;
				foreach ($result as $key => $value) {
					$value->no=$i;
					$new_arr[]=$value;
					$value->emp_date_join = date_format( date_create($value->emp_date_join), 'd M Y');
					$value->emp_finish_contract = date_format( date_create($value->emp_finish_contract), 'd M Y');
					$i++;
				}
				return array('data' => $new_arr);
			});
		}
		

	//
	public function pegawai() {
		$data = generate_page('Data Master Pegawai', 'data_master/pegawai', 'Admin');

			$data_content['title_page'] = 'Data Master Pegawai';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPegawai_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function pegawai_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->pegawai_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$value->tanggal_lahir = date_format( date_create($value->tanggal_lahir), 'd/m/Y');
				$value->avatar = '<img src="' . uploads_url('avatar/'.$value->avatar) . '" alt="image" />';
				$value->tanggal_pengangkatan = date_format( date_create($value->tanggal_pengangkatan), 'd/m/Y');
				$i++;
			}
			return array('data' => $new_arr);
		});
	}

	public function nama_izin() {
		$data = generate_page('Data Master Nama Izin', 'data_master/nama_izin', 'Admin');

			$data_content['title_page'] = 'Data Master Nama Izin';
		$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterIzin_Read', $data_content, true);
		$this->load->view('V_DataMaster_Admin', $data);
	}

	public function namaizin_ajax() {
		json_dump(function() {
			$result= $this->m_datamaster->namaizin_list_all();
			$new_arr=array();$i=1;
			foreach ($result as $key => $value) {
				$value->no=$i;
				$new_arr[]=$value;
				$value->type = '<label class="badge badge-default text-uppercase">'.$value->type.'</label>';
				$i++;
			}
			return array('data' => $new_arr);
		});
	}


		// Import
		public function import() {
			$data = generate_page('Data Master Departement', 'data_master/import', 'Admin');
	
				$data_content['title_page'] = 'Data Master Departement';
			$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterImport_Read', $data_content, true);
			$this->load->view('V_DataMaster_Admin', $data);
		}



	
	/* =================    FUNCTION DELETE
	=================================================================
	*/

	public function delete() {
		if( empty($this->uri->segment('3'))) {
			redirect( base_url('/dashboard') );
		}

		if( empty($this->uri->segment('4'))) {
			redirect( base_url('/dashboard') );
		}

		$name=$this->uri->segment('3');
		$id=$this->uri->segment('4');

		switch ($name) {
			case 'admin':
				$this->m_datamaster->admin_delete($id);
				$this->session->set_flashdata('msg_alert', 'Akun admin berhasil dihapus');
				redirect( base_url('data_master/admin') );
				break;
			case 'jabatan':
				$this->m_datamaster->jabatan_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data jabatan berhasil dihapus');
				redirect( base_url('data_master/jabatan') );
				break;
			
			// Posisi (delete)
			case 'position':
				$this->m_datamaster->position_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data Posisi berhasil dihapus');
				redirect( base_url('data_master/position') );
				break;

			// Departemen (delete)
			case 'dept':
				$this->m_datamaster->dept_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data Departemen berhasil dihapus');
				redirect( base_url('data_master/dept') );
				break;

			// HOD (delete)
			case 'hod':
				$this->m_datamaster->hod_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data HOD berhasil dihapus');
				redirect( base_url('data_master/hod') );
				break;
				
			case 'nilai':
				$this->m_datamaster->bidang_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data bidang berhasil dihapus');
				redirect( base_url('data_master/bidang') );
				break;
			case 'pegawai':
				$this->m_datamaster->pegawai_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data pegawai berhasil dihapus');
				redirect( base_url('data_master/pegawai') );
				break;
			case 'employee':
				$this->m_datamaster->employee_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data Karyawan berhasil dihapus');
				redirect( base_url('data_master/employee') );
				break;
			case 'nama_izin':
				$this->m_datamaster->namaizin_delete($id);
				$this->session->set_flashdata('msg_alert', 'Data nama izin berhasil dihapus');
				redirect( base_url('data_master/nama_izin') );
				break;
			
			default:
				redirect( base_url() );
				break;
		}

	}



	/* =================    FUNCTION EDIT DATA
	=================================================================
	*/

	public function edit() {
		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		if( empty($this->uri->segment('4'))) {
			redirect( base_url() );
		}

		$name=$this->uri->segment('3');
		$id=$this->uri->segment('4');
		
		switch ($name) {
			case 'jabatan':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id_jabatan') );
					$nama_jabatan= $this->security->xss_clean( $this->input->post('nama_jabatan') );
					// validasi
					$this->form_validation->set_rules('id_jabatan', 'ID', 'required');
					$this->form_validation->set_rules('nama_jabatan', 'Nama Jabatan', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->jabatan_update(
						$id,$nama_jabatan
					);
					redirect( base_url('data_master/jabatan') );
				}
				$data = generate_page('Edit Data Master Jabatan', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Jabatan';
					$data_content['data_jabatan'] = $this->m_datamaster->get_data_jabatan($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterJabatan_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			
			// Position (Edit Data)
			case 'position':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('position_id') );
					$position_name= $this->security->xss_clean( $this->input->post('position_name') );
					// validasi
					$this->form_validation->set_rules('position_id', 'ID', 'required');
					$this->form_validation->set_rules('position_name', 'Nama Posisi', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->position_update(
						$id,$position_name
					);
					redirect( base_url('data_master/position') );
				}
				$data = generate_page('Edit Data Master Posisi', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Posisi';
					$data_content['data_position'] = $this->m_datamaster->get_data_position($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPosition_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			// Departemen (Edit Data)
			case 'dept':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('dept_id') );
					$dept_name= $this->security->xss_clean( $this->input->post('dept_name') );
					// validasi
					$this->form_validation->set_rules('dept_id', 'ID', 'required');
					$this->form_validation->set_rules('dept_name', 'Nama Departemen', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->dept_update(
						$id,$dept_name
					);
					redirect( base_url('data_master/dept') );
				}
				$data = generate_page('Edit Data Master Departemen', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Departemen';
					$data_content['data_dept'] = $this->m_datamaster->get_data_dept($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterDept_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			// HOD (Edit Data)
			case 'hod':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('hod_id') );
					$hod_name= $this->security->xss_clean( $this->input->post('hod_name') );
					// validasi
					$this->form_validation->set_rules('hod_id', 'ID', 'required');
					$this->form_validation->set_rules('hod_name', 'Nama HOD', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->hod_update(
						$id,$hod_name
					);
					redirect( base_url('data_master/hod') );
				}
				$data = generate_page('Edit Data Master HOD', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master HOD';
					$data_content['data_hod'] = $this->m_datamaster->get_data_hod($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterHod_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			
			//
			case 'bidang':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id_bidang') );
					$nama_bidang= $this->security->xss_clean( $this->input->post('nama_bidang') );
					// validasi
					$this->form_validation->set_rules('id_bidang', 'ID', 'required');
					$this->form_validation->set_rules('nama_bidang', 'Nama Bidang', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->bidang_update(
						$id,$nama_bidang
					);
					redirect( base_url('data_master/bidang') );
				}
				$data = generate_page('Edit Data Master Bidang', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Bidang';
					$data_content['data_bidang'] = $this->m_datamaster->get_data_bidang($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterBidang_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			case 'nama_izin':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_namaizin= $this->security->xss_clean( $this->input->post('id_namaizin') );
					$type= $this->security->xss_clean( $this->input->post('type') );
					$nama_izin= $this->security->xss_clean( $this->input->post('nama_izin') );
					// validasi
					$this->form_validation->set_rules('id_namaizin', 'ID', 'required');
					$this->form_validation->set_rules('type', 'Type Izin', 'required');
					$this->form_validation->set_rules('nama_izin', 'Nama Izin', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->namaizin_update(
						$id_namaizin,$type,$nama_izin
					);
					//redirect( base_url('data_master/nama_izin') );
				}
				$data = generate_page('Edit Data Master Nama Izin', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Nama Izin';
					$data_content['data_namaizin'] = $this->m_datamaster->get_data_namaizin($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterIzin_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			case 'pegawai':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id= $this->security->xss_clean( $this->input->post('id') );
					$nama= $this->security->xss_clean( $this->input->post('nama') );
					$nip= $this->security->xss_clean( $this->input->post('nip') );
					$tempat_lahir= $this->security->xss_clean( $this->input->post('tempat_lahir') );
					$tanggal_lahir= $this->security->xss_clean( $this->input->post('tanggal_lahir') );
					$jenis_kelamin= $this->security->xss_clean( $this->input->post('jenis_kelamin') );
					$pendidikan_terakhir= $this->security->xss_clean( $this->input->post('pendidikan_terakhir') );
					$status_perkawinan= $this->security->xss_clean( $this->input->post('status_perkawinan') );
					$status_pegawai= $this->security->xss_clean( $this->input->post('status_pegawai') );
					$id_jabatan= $this->security->xss_clean( $this->input->post('id_jabatan') );
					$id_bidang= $this->security->xss_clean( $this->input->post('id_bidang') );
					$agama= $this->security->xss_clean( $this->input->post('agama') );
					$alamat= $this->security->xss_clean( $this->input->post('alamat') );
					$no_ktp= $this->security->xss_clean( $this->input->post('no_ktp') );
					$no_rumah= $this->security->xss_clean( $this->input->post('no_rumah') );
					$no_handphone= $this->security->xss_clean( $this->input->post('no_handphone') );
					$email= $this->security->xss_clean( $this->input->post('email') );
					$password= $this->security->xss_clean( $this->input->post('password') );
					$id_user= $this->security->xss_clean( $this->input->post('id_user') );
					$tanggal_pengangkatan= $this->security->xss_clean( $this->input->post('tanggal_pengangkatan') );
					$avatar = '';
					// avatar
					if ( $this->security->xss_clean( $_FILES["avatar"] ) && $_FILES['avatar']['name'] ) {
						$config['upload_path']          = './uploads/avatar/';
						$config['allowed_types']        = 'jpg|jpeg|png';
						$config['max_size']             = 2000;
						$config['file_name']			= md5(time() . '_' . $_FILES["avatar"]['name']);
				 
						$this->load->library('upload', $config);
				 
						if ( !$this->upload->do_upload('avatar') && !empty($_FILES['avatar']['name'])) {
							$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						} else {
							$avatar = $this->upload->data()['file_name'];
						}
					}
					// validasi
					$this->form_validation->set_rules('id', 'ID User', 'required');
					$this->form_validation->set_rules('nama', 'Nama', 'required');
					$this->form_validation->set_rules('nip', 'NIP', 'required');
					$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
					$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
					$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
					$this->form_validation->set_rules('pendidikan_terakhir', 'Pendidikan Terakhir', 'required');
					$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required');
					$this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
					$this->form_validation->set_rules('id_jabatan', 'Jabatan', 'required');
					$this->form_validation->set_rules('id_bidang', 'Bidang', 'required');
					$this->form_validation->set_rules('agama', 'Agama', 'required');
					$this->form_validation->set_rules('alamat', 'Alamat', 'required');
					$this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
					$this->form_validation->set_rules('no_rumah', 'No Rumah', 'required');
					$this->form_validation->set_rules('no_handphone', 'No Handphone', 'required');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('password', 'Password', '');
					$this->form_validation->set_rules('id_user', 'ID User', 'required');
					$this->form_validation->set_rules('tanggal_pengangkatan', 'Tanggal Pengangkatan', 'required');
					
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					// to-do
					$this->m_datamaster->pegawai_update(
					$id,$nama,$nip,$tempat_lahir,$tanggal_lahir,$jenis_kelamin,$pendidikan_terakhir,$status_perkawinan,
					$status_pegawai,$id_jabatan,$id_bidang,$agama,$alamat,$no_ktp,$no_rumah,$no_handphone,$email,
					$password,$id_user,$tanggal_pengangkatan,$avatar
					);
					redirect( base_url('data_master/pegawai') );
				}
				$data = generate_page('Edit Data Master Pegawai', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['list_bidang'] = $this->m_datamaster->get_list_bidang();
					$data_content['list_jabatan'] = $this->m_datamaster->get_list_jabatan();
					$data_content['title_page'] = 'Edit Data Master Pegawai';
					$data_content['data_pegawai'] = $this->m_datamaster->get_data_pegawai($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPegawai_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			case 'admin':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$id_user= $this->security->xss_clean( $this->input->post('id_user') );
					$username= $this->security->xss_clean( $this->input->post('username') );
					$email= $this->security->xss_clean( $this->input->post('email') );
					$namalengkap= $this->security->xss_clean( $this->input->post('namalengkap') );
					$password= $this->security->xss_clean( $this->input->post('password') );
					$type= $this->security->xss_clean( $this->input->post('type') );
					$avatar = '';
					// avatar
					if ( $this->security->xss_clean( $_FILES["avatar"] ) && $_FILES['avatar']['name'] ) {
						$config['upload_path']          = './uploads/avatar/';
						$config['allowed_types']        = 'jpg|jpeg|png';
						$config['max_size']             = 2000;
						$config['file_name']			= md5(time() . '_' . $_FILES["avatar"]['name']);
			
						$this->load->library('upload', $config);
			
						if ( !$this->upload->do_upload('avatar') && !empty($_FILES['avatar']['name'])) {
							$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						} else {
							$avatar = $this->upload->data()['file_name'];
						}
					}
					// validasi
					$this->form_validation->set_rules('id_user', 'id_user', 'required');
					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
					$this->form_validation->set_rules('password', 'Password', '');
					$this->form_validation->set_rules('type', 'Type', 'required');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/edit/' . $name . '/' . $id) );
					}
					$this->m_datamaster->admin_update(
						$id_user, $username, $email, $namalengkap, $password, $type, $avatar
					);
					redirect( base_url('data_master/' . $name) );
				}

				$data = generate_page('Edit Data Master Admin', 'data_master/edit/' . $name . '/' . $id, 'Admin');

					$data_content['title_page'] = 'Edit Data Master Admin';
					$data_content['data_admin'] = $this->m_datamaster->get_data_admin($id);
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterAdmin_Edit', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

				// Employee
				case 'employee':
					if( $_SERVER['REQUEST_METHOD'] == 'POST') {
						$emp_id= $this->security->xss_clean( $this->input->post('emp_id') );
						$position_id= $this->security->xss_clean( $this->input->post('position_id') );
						$emp_badge_number= $this->security->xss_clean( $this->input->post('emp_badge_number') );
						$emp_name= $this->security->xss_clean( $this->input->post('emp_name') );
						$emp_date_join= $this->security->xss_clean( $this->input->post('emp_date_join') );
						$emp_finish_contract= $this->security->xss_clean( $this->input->post('emp_finish_contract') );
						$emp_phone_number= $this->security->xss_clean( $this->input->post('emp_phone_number') );
						$emp_whatsapp_number= $this->security->xss_clean( $this->input->post('emp_whatsapp_number') );
						$emp_work_bpjs_number= $this->security->xss_clean( $this->input->post('emp_work_bpjs_number') );
						$emp_address= $this->security->xss_clean( $this->input->post('emp_address') );
						$emp_npwp_number= $this->security->xss_clean( $this->input->post('emp_npwp_number') );
						$dept_id= $this->security->xss_clean( $this->input->post('dept_id') );
						$hod_id= $this->security->xss_clean( $this->input->post('hod_id') );
						$emp_health_bpjs_number= $this->security->xss_clean( $this->input->post('emp_health_bpjs_number') );
						$emp_medical_by= $this->security->xss_clean( $this->input->post('emp_medical_by') );
						$emp_email= $this->security->xss_clean( $this->input->post('emp_email') );
						$emp_bank_rek= $this->security->xss_clean( $this->input->post('emp_bank_rek') );
						$emp_employee_status= $this->security->xss_clean( $this->input->post('emp_employee_status') );
						$emp_status= $this->security->xss_clean( $this->input->post('emp_status') );
						$emp_place_birth= $this->security->xss_clean( $this->input->post('emp_place_birth') );
						$emp_date_of_birth= $this->security->xss_clean( $this->input->post('emp_date_of_birth') );
						$emp_ktp= $this->security->xss_clean( $this->input->post('emp_ktp') );
						$emp_age= $this->security->xss_clean( $this->input->post('emp_age') );
						$emp_gender= $this->security->xss_clean( $this->input->post('emp_gender') );
						$emp_religion= $this->security->xss_clean( $this->input->post('emp_religion') );
						$emp_hobby= $this->security->xss_clean( $this->input->post('emp_hobby') );
						$emp_mariage_status= $this->security->xss_clean( $this->input->post('emp_mariage_status') );
						$emp_urgent_name= $this->security->xss_clean( $this->input->post('emp_urgent_name') );
						$emp_urgent_mobile_number= $this->security->xss_clean( $this->input->post('emp_urgent_mobile_number') );
						$emp_urgent_address= $this->security->xss_clean( $this->input->post('emp_urgent_address') );

						$edu_last= $this->security->xss_clean( $this->input->post('edu_last') );
						$edu_year= $this->security->xss_clean( $this->input->post('edu_year') );
						$edu_tittle= $this->security->xss_clean( $this->input->post('edu_tittle') );
						$edu_dept= $this->security->xss_clean( $this->input->post('edu_dept') );

						$work_year= $this->security->xss_clean( $this->input->post('work_year') );
						$work_company= $this->security->xss_clean( $this->input->post('work_company') );
						$work_position= $this->security->xss_clean( $this->input->post('work_position') );
						$work_sallary= $this->security->xss_clean( $this->input->post('work_sallary') );
						$work_reason= $this->security->xss_clean( $this->input->post('work_reason') );

						$work_year2= $this->security->xss_clean( $this->input->post('work_year2') );
						$work_company2= $this->security->xss_clean( $this->input->post('work_company2') );
						$work_position2= $this->security->xss_clean( $this->input->post('work_position2') );
						$work_sallary2= $this->security->xss_clean( $this->input->post('work_sallary2') );
						$work_reason2= $this->security->xss_clean( $this->input->post('work_reason2') );

						$emp_img = '';
						// avatar
						if ( $this->security->xss_clean( $_FILES["emp_img"] ) && $_FILES['emp_img']['name'] ) {
							$config['upload_path']          = './uploads/avatar/';
							$config['allowed_types']        = 'jpg|jpeg|png';
							$config['max_size']             = 2000;
							$config['file_name']			= md5(time() . '_' . $_FILES["emp_img"]['name']);
					
							$this->load->library('upload', $config);
					
							if ( !$this->upload->do_upload('emp_img') && !empty($_FILES['emp_img']['name'])) {
								$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
								redirect( base_url('data_master/edit/' . $name . '/' . $id) );
							} else {
								$avatar = $this->upload->data()['file_name'];
							}
						}
						// validasi
						$this->form_validation->set_rules('emp_id', 'ID User', 'required');
						$this->form_validation->set_rules('position_id', 'Position');
						$this->form_validation->set_rules('emp_badge_number', 'No. Karyawan');
						$this->form_validation->set_rules('emp_name', 'Nama Lengkap');
						$this->form_validation->set_rules('emp_date_join', 'Tanggal Masuk');
						$this->form_validation->set_rules('emp_finish_contract', 'Kontrak Berakhir');
						$this->form_validation->set_rules('emp_phone_number', 'No. HP');
						$this->form_validation->set_rules('emp_whatsapp_number', 'No. Whatsapp');
						$this->form_validation->set_rules('emp_work_bpjs_number', 'No. BPJS Ketenagakerjaan');
						$this->form_validation->set_rules('emp_address', 'Alamat');
						$this->form_validation->set_rules('emp_npwp_number', 'No. NPWP');
						$this->form_validation->set_rules('dept_id', 'Departemen');
						$this->form_validation->set_rules('hod_id', 'HOD');
						$this->form_validation->set_rules('emp_health_bpjs_number', 'No. BPJS Kesehatan');
						$this->form_validation->set_rules('emp_medical_by', 'Medical By');
						$this->form_validation->set_rules('emp_email', 'Email');
						$this->form_validation->set_rules('emp_bank_rek', 'No. Rekening Bank');
						$this->form_validation->set_rules('emp_employee_status', 'Status Kontrak');
						$this->form_validation->set_rules('emp_status', 'Status Karyawan');
						$this->form_validation->set_rules('emp_place_birth', 'Tempat Lahir');
						$this->form_validation->set_rules('emp_date_of_birth', 'Tanggal Lahir');
						$this->form_validation->set_rules('emp_ktp', 'No. KTP');
						$this->form_validation->set_rules('Umur', 'No. KTP');
						$this->form_validation->set_rules('emp_gender', 'Jenis Kelamin');
						$this->form_validation->set_rules('emp_religion', 'Agama');
						$this->form_validation->set_rules('emp_hobby', 'Hobi');
						$this->form_validation->set_rules('emp_mariage_status', 'Status Perkawinan');
						$this->form_validation->set_rules('emp_urgent_name', 'Nama');
						$this->form_validation->set_rules('emp_urgent_mobile_number', 'No. HP');
						$this->form_validation->set_rules('emp_urgent_address', 'Alamat');

						$this->form_validation->set_rules('edu_last', 'Pendidikan Akhir/ Kursus');
						$this->form_validation->set_rules('edu_year', 'Tahun / Year');
						$this->form_validation->set_rules('edu_tittle', 'Gelar yang diperoleh');
						$this->form_validation->set_rules('edu_dept', 'Jurusan');
	
						$this->form_validation->set_rules('work_year', 'Tahun');
						$this->form_validation->set_rules('work_company', 'Nama Perusahaan');
						$this->form_validation->set_rules('work_position', 'Jabatan');
						$this->form_validation->set_rules('work_sallary', 'Gaji Pokok/Sallary');
						$this->form_validation->set_rules('work_reason', 'Alasan berhenti bekerja');
	
						$this->form_validation->set_rules('work_year2', 'Tahun');
						$this->form_validation->set_rules('work_company2', 'Nama Perusahaan');
						$this->form_validation->set_rules('work_position2', 'Jabatan');
						$this->form_validation->set_rules('work_sallary2', 'Gaji Pokok/Sallary');
						$this->form_validation->set_rules('work_reason2', 'Alasan berhenti bekerja');
	
						if(!$this->form_validation->run()) {
							$this->session->set_flashdata('msg_alert', validation_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						}
						$this->m_datamaster->employee_update(
							$emp_id, $position_id, $emp_badge_number, $emp_name, $emp_date_join, $emp_finish_contract, $emp_phone_number, $emp_whatsapp_number, $emp_work_bpjs_number, 
							$emp_address, $emp_npwp_number, $dept_id, $hod_id, $emp_health_bpjs_number,$emp_email,$emp_bank_rek,$emp_employee_status,$emp_status,$emp_place_birth,
							$emp_date_of_birth,$emp_ktp,$emp_age,$emp_gender,$emp_religion,$emp_hobby,$emp_mariage_status,$emp_urgent_name,$emp_urgent_mobile_number,
							$emp_urgent_address, $emp_medical_by,
							$edu_last,$edu_year,$edu_tittle,$edu_dept,
							$work_year,$work_company,$work_position,$work_sallary,$work_reason,
							$work_year2,$work_company2,$work_position2,$work_sallary2,$work_reason2,
							$emp_img
						);
						redirect( base_url('data_master/' . $name) );
					}
	
					$data = generate_page('Edit Data Master Karyawan', 'data_master/edit/' . $name . '/' . $id, 'Admin');

						$data_content['list_position'] = $this->m_datamaster->get_list_position();
						$data_content['list_dept'] = $this->m_datamaster->get_list_dept();
						$data_content['list_hod'] = $this->m_datamaster->get_list_hod();
						$data_content['title_page'] = 'Edit Data Master Karyawan';
						$data_content['data_employee'] = $this->m_datamaster->get_data_employee($id);
					$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterEmployee_Edit', $data_content, true);
					$this->load->view('V_DataMaster_Admin', $data);
					break;
		}
	}




	/* =================    FUNCTION ADD NEW
	=================================================================
	*/

	public function add_new() {

		if( empty($this->uri->segment('3'))) {
			redirect( base_url() );
		}

		$name=$this->uri->segment('3');
			
		switch ($name) {
			case 'jabatan':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$nama_jabatan= $this->security->xss_clean( $this->input->post('nama_jabatan') );
					$this->form_validation->set_rules('nama_jabatan', 'Nama Jabatan', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->jabatan_add_new(
						$nama_jabatan
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Entry Data Master Jabatan', 'data_master/add_new/jabatan', 'Admin');

					$data_content['title_page'] = 'Entry Data Master Jabatan';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterJabatan_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			case 'bidang':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$nama_bidang= $this->security->xss_clean( $this->input->post('nama_bidang') );
					$this->form_validation->set_rules('nama_bidang', 'Nama Bidang', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->bidang_add_new(
						$nama_bidang
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Entry Data Master Bidang', 'data_master/add_new/bidang', 'Admin');

					$data_content['title_page'] = 'Entry Data Master Bidang';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterBidang_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			
			// Position
			case 'position':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$position_name= $this->security->xss_clean( $this->input->post('position_name') );
					$this->form_validation->set_rules('position_name', 'Nama Posisi', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->position_add_new(
						$position_name
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Tambah Data Master Posisi', 'data_master/add_new/position', 'Admin');

					$data_content['title_page'] = 'Tambah Data Master Posisi';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPosition_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			// Departemen
			case 'dept':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$dept_name= $this->security->xss_clean( $this->input->post('dept_name') );
					$this->form_validation->set_rules('dept_name', 'Nama Departemen', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->dept_add_new(
						$dept_name
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Tambah Data Master Departemen', 'data_master/add_new/dept', 'Admin');

					$data_content['title_page'] = 'Tambah Data Master Departemen';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterDept_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			// HOD
			case 'hod':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$hod_name= $this->security->xss_clean( $this->input->post('hod_name') );
					$this->form_validation->set_rules('hod_name', 'Nama HOD', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->hod_add_new(
						$hod_name
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Tambah Data Master HOD', 'data_master/add_new/hod', 'Admin');

					$data_content['title_page'] = 'Tambah Data Master HOD';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterHod_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			//
			case 'nama_izin':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$type= $this->security->xss_clean( $this->input->post('type') );
					$nama_izin= $this->security->xss_clean( $this->input->post('nama_izin') );
					$this->form_validation->set_rules('type', 'Type Izin', 'required');
					$this->form_validation->set_rules('nama_izin', 'Nama Izin', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->namaizin_add_new(
						$type,$nama_izin
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Entry Data Master Nama Izin', 'data_master/add_new/nama_izin', 'Admin');

					$data_content['title_page'] = 'Entry Data Master Nama Izin';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterIzin_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			case 'admin':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$username= $this->security->xss_clean( $this->input->post('username') );
					$email= $this->security->xss_clean( $this->input->post('email') );
					$namalengkap= $this->security->xss_clean( $this->input->post('namalengkap') );
					$password= $this->security->xss_clean( $this->input->post('password') );
					$type= $this->security->xss_clean( $this->input->post('type') );
					$avatar = '';
					// avatar
					if ( $this->security->xss_clean( $_FILES["avatar"] ) && $_FILES['avatar']['name'] ) {
						$config['upload_path']          = './uploads/avatar/';
						$config['allowed_types']        = 'jpg|jpeg|png';
						$config['max_size']             = 2000;
						$config['file_name']			= md5(time() . '_' . $_FILES["avatar"]['name']);
				 
						$this->load->library('upload', $config);
				 
						if ( !$this->upload->do_upload('avatar') && !empty($_FILES['avatar']['name'])) {
							$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						} else {
							$avatar = $this->upload->data()['file_name'];
						}
					}
					// validasi
					$this->form_validation->set_rules('username', 'Username', 'required');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('namalengkap', 'Nama Lengkap', 'required');
					$this->form_validation->set_rules('password', 'Password', 'required');
					$this->form_validation->set_rules('type', 'Type', 'required');
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->admin_add_new(
						$username, $email, $namalengkap, $password, $type, $avatar
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Entry Data Master Admin', 'data_master/add_new/admin', 'Admin');

					$data_content['title_page'] = 'Entry Data Master Admin';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterAdmin_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
			
			// Employee
			case 'employee':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$dept_id= $this->security->xss_clean( $this->input->post('dept_id') );
					$position_id= $this->security->xss_clean( $this->input->post('position_id') );
					$emp_badge_number= $this->security->xss_clean( $this->input->post('emp_badge_number') );
					$emp_name= $this->security->xss_clean( $this->input->post('emp_name') );
					$emp_date_join= $this->security->xss_clean( $this->input->post('emp_date_join') );
					$emp_phone_number= $this->security->xss_clean( $this->input->post('emp_phone_number') );
					$emp_work_bpjs_number= $this->security->xss_clean( $this->input->post('emp_work_bpjs_number') );
					$emp_address= $this->security->xss_clean( $this->input->post('emp_address') );
					$emp_npwp_number= $this->security->xss_clean( $this->input->post('emp_npwp_number') );

					$emp_place_birth= $this->security->xss_clean( $this->input->post('emp_place_birth') );
					$emp_date_of_birth= $this->security->xss_clean( $this->input->post('emp_date_of_birth') );
					$emp_ktp= $this->security->xss_clean( $this->input->post('emp_ktp') );
					$emp_age= $this->security->xss_clean( $this->input->post('emp_age') );
					$emp_gender= $this->security->xss_clean( $this->input->post('emp_gender') );
					$emp_religion= $this->security->xss_clean( $this->input->post('emp_religion') );
					$emp_hobby= $this->security->xss_clean( $this->input->post('emp_hobby') );
					$emp_mariage_status= $this->security->xss_clean( $this->input->post('emp_mariage_status') );

					$emp_urgent_name= $this->security->xss_clean( $this->input->post('emp_urgent_name') );
					$emp_urgent_mobile_number= $this->security->xss_clean( $this->input->post('emp_urgent_mobile_number') );
					$emp_urgent_address= $this->security->xss_clean( $this->input->post('emp_urgent_address') );

					$edu_last= $this->security->xss_clean( $this->input->post('edu_last') );
					$edu_year= $this->security->xss_clean( $this->input->post('edu_year') );
					$edu_tittle= $this->security->xss_clean( $this->input->post('edu_tittle') );
					$edu_dept= $this->security->xss_clean( $this->input->post('edu_dept') );

					$work_year= $this->security->xss_clean( $this->input->post('work_year') );
					$work_company= $this->security->xss_clean( $this->input->post('work_company') );
					$work_position= $this->security->xss_clean( $this->input->post('work_position') );
					$work_sallary= $this->security->xss_clean( $this->input->post('work_sallary') );
					$work_reason= $this->security->xss_clean( $this->input->post('work_reason') );
					
					$work_year2= $this->security->xss_clean( $this->input->post('work_year2') );
					$work_company2= $this->security->xss_clean( $this->input->post('work_company2') );
					$work_position2= $this->security->xss_clean( $this->input->post('work_position2') );
					$work_sallary2= $this->security->xss_clean( $this->input->post('work_sallary2') );
					$work_reason2= $this->security->xss_clean( $this->input->post('work_reason2') );

					$inf_1= $this->security->xss_clean( $this->input->post('inf_1') );
					$inf_2= $this->security->xss_clean( $this->input->post('inf_2') );
					$inf_3= $this->security->xss_clean( $this->input->post('inf_3') );
					$inf_4= $this->security->xss_clean( $this->input->post('inf_4') );
					$inf_5= $this->security->xss_clean( $this->input->post('inf_5') );
					$inf_6= $this->security->xss_clean( $this->input->post('inf_6') );

					$emp_img = '';
					// avatar
					if ( $this->security->xss_clean( $_FILES["emp_img"] ) && $_FILES['emp_img']['name'] ) {
						$config['upload_path']          = './uploads/avatar/';
						$config['allowed_types']        = 'jpg|jpeg|png';
						$config['max_size']             = 2000;
						$config['file_name']			= md5(time() . '_' . $_FILES["emp_img"]['name']);
				 
						$this->load->library('upload', $config);
				 
						if ( !$this->upload->do_upload('emp_img') && !empty($_FILES['emp_img']['name'])) {
							$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						} else {
							$emp_img = $this->upload->data()['file_name'];
						}
					}
					// validasi
					$this->form_validation->set_rules('dept_id', 'Departemen', 'required');
					$this->form_validation->set_rules('position_id', 'Position', 'required');
					$this->form_validation->set_rules('emp_badge_number', 'No. Karyawan', 'required');
					$this->form_validation->set_rules('emp_name', 'Nama Lengkap', 'required');
					$this->form_validation->set_rules('emp_date_join', 'Tanggal Masuk');
					$this->form_validation->set_rules('emp_phone_number', 'No. HP');
					$this->form_validation->set_rules('emp_work_bpjs_number', 'No. BPJS Ketenagakerjaan');
					$this->form_validation->set_rules('emp_address', 'Alamat');
					$this->form_validation->set_rules('emp_npwp_number', 'No. NPWP');

					$this->form_validation->set_rules('emp_place_birth', 'Tempat lahir');
					$this->form_validation->set_rules('emp_date_of_birth', 'Tanggal Lahir');
					$this->form_validation->set_rules('emp_ktp', 'No. KTP');
					$this->form_validation->set_rules('emp_age', 'Umur');
					$this->form_validation->set_rules('emp_gender', 'Jenis Kelamin');
					$this->form_validation->set_rules('emp_religion', 'Agama');
					$this->form_validation->set_rules('emp_hobby', 'Hobi');
					$this->form_validation->set_rules('emp_mariage_status', 'Status Perkawinan');

					$this->form_validation->set_rules('emp_urgent_name', 'Nama');
					$this->form_validation->set_rules('emp_urgent_mobile_number', 'No. HP');
					$this->form_validation->set_rules('emp_urgent_address', 'Alamat');

					$this->form_validation->set_rules('edu_last', 'Pendidikan Akhir/ Kursus');
          $this->form_validation->set_rules('edu_year', 'Tahun / Year');
          $this->form_validation->set_rules('edu_tittle', 'Gelar yang diperoleh');
          $this->form_validation->set_rules('edu_dept', 'Jurusan');

          $this->form_validation->set_rules('work_year', 'Tahun');
          $this->form_validation->set_rules('work_company', 'Nama Perusahaan');
          $this->form_validation->set_rules('work_position', 'Jabatan');
          $this->form_validation->set_rules('work_sallary', 'Gaji Pokok/Sallary');
          $this->form_validation->set_rules('work_reason', 'Alasan berhenti bekerja');

          $this->form_validation->set_rules('work_year2', 'Tahun');
          $this->form_validation->set_rules('work_company2', 'Nama Perusahaan');
          $this->form_validation->set_rules('work_position2', 'Jabatan');
          $this->form_validation->set_rules('work_sallary2', 'Gaji Pokok/Sallary');
          $this->form_validation->set_rules('work_reason2', 'Alasan berhenti bekerja');

          $this->form_validation->set_rules('inf_1', 'Apakah anda bisa bekerja pada malam hari?');
          $this->form_validation->set_rules('inf_2', 'Apakah anda siap untuk kerja lembur bila diperintah atasan?');
          $this->form_validation->set_rules('inf_3', 'Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?');
          $this->form_validation->set_rules('inf_4', 'Apakah anda sudah membaca dan memahami modul-modul Training yang diberikan?');
          $this->form_validation->set_rules('inf_5', 'Apakah anda sudah pernah bekerja di PT. Suntech Plastics sebelumnya?');
          $this->form_validation->set_rules('inf_6', 'Besar gaji yang anda inginkan? Rp . . . .');

					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					$this->m_datamaster->employee_add_new(
						$dept_id, $position_id, $emp_badge_number, $emp_name, $emp_date_join, $emp_phone_number, $emp_work_bpjs_number, 
						$emp_address, $emp_npwp_number, $emp_place_birth, $emp_date_of_birth, $emp_ktp, $emp_age, $emp_gender, $emp_religion, $emp_hobby, 
						$emp_mariage_status, $emp_urgent_name, $emp_urgent_mobile_number, $emp_urgent_address,
            $edu_last,$edu_year,$edu_tittle,$edu_dept,
            $work_year,$work_company,$work_position,$work_sallary,$work_reason,
            $work_year2,$work_company2,$work_position2,$work_sallary2,$work_reason2,
            $inf_1,$inf_2,$inf_3,$inf_4,$inf_5,$inf_6, 
            $emp_img
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Input Data Master Karyawan', 'data_master/add_new/employee', 'Admin');

					$data_content['title_page'] = 'Input Data Master Karyawan';
					$data_content['list_dept'] = $this->m_datamaster->get_list_dept();
					$data_content['list_position'] = $this->m_datamaster->get_list_position();
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterEmployee_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;

			case 'pegawai':
				if( $_SERVER['REQUEST_METHOD'] == 'POST') {
					$nama= $this->security->xss_clean( $this->input->post('nama') );
					$nip= $this->security->xss_clean( $this->input->post('nip') );
					$tempat_lahir= $this->security->xss_clean( $this->input->post('tempat_lahir') );
					$tanggal_lahir= $this->security->xss_clean( $this->input->post('tanggal_lahir') );
					$jenis_kelamin= $this->security->xss_clean( $this->input->post('jenis_kelamin') );
					$pendidikan_terakhir= $this->security->xss_clean( $this->input->post('pendidikan_terakhir') );
					$status_perkawinan= $this->security->xss_clean( $this->input->post('status_perkawinan') );
					$status_pegawai= $this->security->xss_clean( $this->input->post('status_pegawai') );
					$id_jabatan= $this->security->xss_clean( $this->input->post('id_jabatan') );
					$id_bidang= $this->security->xss_clean( $this->input->post('id_bidang') );
					$agama= $this->security->xss_clean( $this->input->post('agama') );
					$alamat= $this->security->xss_clean( $this->input->post('alamat') );
					$no_ktp= $this->security->xss_clean( $this->input->post('no_ktp') );
					$no_rumah= $this->security->xss_clean( $this->input->post('no_rumah') );
					$no_handphone= $this->security->xss_clean( $this->input->post('no_handphone') );
					$email= $this->security->xss_clean( $this->input->post('email') );
					$password= $this->security->xss_clean( $this->input->post('password') );
					$id_user= $this->security->xss_clean( $this->input->post('id_user') );
					$tanggal_pengangkatan= $this->security->xss_clean( $this->input->post('tanggal_pengangkatan') );
					$avatar = '';
					// avatar
					if ( $this->security->xss_clean( $_FILES["avatar"] ) && $_FILES['avatar']['name'] ) {
						$config['upload_path']          = './uploads/avatar/';
						$config['allowed_types']        = 'jpg|jpeg|png';
						$config['max_size']             = 2000;
						$config['file_name']			= md5(time() . '_' . $_FILES["avatar"]['name']);
				 
						$this->load->library('upload', $config);
				 
						if ( !$this->upload->do_upload('avatar') && !empty($_FILES['avatar']['name'])) {
							$this->session->set_flashdata('msg_alert', $this->upload->display_errors());
							redirect( base_url('data_master/edit/' . $name . '/' . $id) );
						} else {
							$avatar = $this->upload->data()['file_name'];
						}
					}
					// validasi
					$this->form_validation->set_rules('nama', 'Nama', 'required');
					$this->form_validation->set_rules('nip', 'NIP', 'required');
					$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
					$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
					$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
					$this->form_validation->set_rules('pendidikan_terakhir', 'Pendidikan Terakhir', 'required');
					$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required');
					$this->form_validation->set_rules('status_pegawai', 'status_pegawai', 'required');
					$this->form_validation->set_rules('id_jabatan', 'Jabatan', 'required');
					$this->form_validation->set_rules('id_bidang', 'Bidang', 'required');
					$this->form_validation->set_rules('agama', 'Agama', 'required');
					$this->form_validation->set_rules('alamat', 'Alamat', 'required');
					$this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
					$this->form_validation->set_rules('no_rumah', 'No Rumah', 'required');
					$this->form_validation->set_rules('no_handphone', 'No Handphone', 'required');
					$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
					$this->form_validation->set_rules('password', 'Password', 'required');
					$this->form_validation->set_rules('id_user', 'ID User', 'required');
					$this->form_validation->set_rules('tanggal_pengangkatan', 'Tanggal Pengangkatan', 'required');
					
					if(!$this->form_validation->run()) {
						$this->session->set_flashdata('msg_alert', validation_errors());
						redirect( base_url('data_master/add_new/' . $name) );
					}
					// to-do
					$this->m_datamaster->pegawai_add_new(
					$nama,$nip,$tempat_lahir,$tanggal_lahir,$jenis_kelamin,$pendidikan_terakhir,$status_perkawinan,
					$status_pegawai,$id_jabatan,$id_bidang,$agama,$alamat,$no_ktp,$no_rumah,$no_handphone,$email,
					$password,$id_user,$tanggal_pengangkatan,$avatar
					);
					redirect( base_url('data_master/' . $name) );
				}
				$data = generate_page('Entry Data Master Pegawai', 'data_master/add_new/pegawai', 'Admin');

					$data_content['list_bidang'] = $this->m_datamaster->get_list_bidang();
					$data_content['list_jabatan'] = $this->m_datamaster->get_list_jabatan();
					$data_content['title_page'] = 'Entry Data Master Pegawai';
				$data['content'] = $this->load->view('partial/DataMasterAdmin/V_Admin_DataMasterPegawai_Create', $data_content, true);
				$this->load->view('V_DataMaster_Admin', $data);
				break;
		}
	}



}

/* End of file Data_Master.php */
/* Location: ./application/controllers/Data_Master.php */