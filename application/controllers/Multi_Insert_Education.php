<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Multi_Insert_Education extends CI_Controller {
 
   function __construct()
    {
        parent::__construct();
 
        $this->load->model('app');
    }
 
    public function index()
    {
      //jika button simpan di klik
      if ($this->input->post('submit', TRUE) == 'submit') {
 
         $post = $this->input->post();
 
         foreach ($post['education_last'] as $key => $value) {
            //masukkan data ke variabel array jika kedua form tidak kosong
            if ($post['education_last'][$key] != '' || $post['education_year'][$key] != '')
            {
               $simpan[] = array(
                  'education_last' => $post['education_last'][$key],
                  'education_year'         => $post['education_year'][$key]
               );
            }
         }
 
         //simpan data
         $this->app->multi_insert('stc_education', $simpan);
         //redirect
         redirect('multi_insert');
 
      }
      //ambil data
      $data['data'] = $this->app->get('stc_education');
      //load view
        $this->load->view('index', $data);
    }
}